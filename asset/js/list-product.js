$(document).ready(function(){
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    const vtoken = getCookie("token");
    var urlInfo = "http://localhost:8080/shop24h/api/noauth/customers";

    var gUrlProduct = "http://localhost:8080/shop24h/api/noauth/products";

    var gUrlFilterProduct = "http://localhost:8080/shop24h/api/noauth/products/product-props"

    var gUrlProductByProductName = "http://localhost:8080/shop24h/api/noauth/products/product-name/";

    var gUrlWishlist = "http://localhost:8080/shop24h/wishlists";

    var gUrlOffice = "http://localhost:8080/shop24h/api/noauth/offices";

    var gUrlDownloadPhoto = "http://localhost:8080/shop24h/api/noauth/downloadFile";
    
    var gNumberProduct = $("#number-product");
    
    var gUser = localStorage.getItem("user");

    var gProductListId = JSON.parse(localStorage.getItem("productList"));

    var gBrandSelected = "all";

    var gCategories = "all";

    var gPrice1 = 0;

    var gPrice2 = 99999999999999;

    var gOnSale = 0;

    var gInstock = -1;

    var gSearch = "all";
    // Trang hiện tại
    var gCurrentPage = 1;

    // Scroll interval
    const gSrTop = ScrollReveal({
        origin: "top",
        distance : "80px",
        duration : 1000,
        reset: false
    })

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading();

    // Scroll reveal
    gSrTop.reveal(".path")
    gSrTop.reveal(".brand")
    gSrTop.reveal(".filter",{delay: 300})

    // Tạo sự kiện nhấn nút tìm kiếm sản phẩm
    $(document).on("input change propertychange paste keyup", "#inp-search", function(){
        gSearch = $("#inp-search").val().trim();
        if($("#inp-search").val().trim() == ""){
            gSearch = "all";
        }
        gCurrentPage = 1;
        callApiFilterProduct(0,12);
    })
    // Tạo sự kiện nhập ô input thay đổi giá
    $(document).on("input change propertychange paste keyup", "#inp-price1", function(){
        gPrice1 = $("#inp-price1").val();
        if(gPrice1 == ""){
            gPrice1 = 0;
        }
    })
    // Tạo sự kiện nhập ô input thay đổi giá
    $(document).on("input change propertychange paste keyup", "#inp-price2", function(){
        gPrice2 = $("#inp-price2").val();
        if(gPrice2 == ""){
            gPrice2 = 99999999999999;
        }
    })
    // Tạo sự kiện đóng mở filter
    $(document).on("click", "#filter-icon", function(){
        $(".filter").attr("style", "transform: translateX(-0%);")
    })
    // Tạo sự kiện đóng mở filter
    $(document).on("click", "#close-filter", function(){
        $(".filter").attr("style", "transform: translateX(-120%);")
    })

    // Tạo sự kiện nhấn nút checkbox onsale 
    $(document).on("change", "#checkbox-sale", function(){
        if ($("#checkbox-sale").is(":checked")){
            gOnSale = 1;
            gCurrentPage = 1;
            callApiFilterProduct(0,12);
        }
        if ($("#checkbox-sale").is(":checked") == false){
            gOnSale = 0;
            gCurrentPage = 1;
            callApiFilterProduct(0,12);
        }
        
    })
    // Tạo sự kiện nhấn nút checkbox instock 
    $(document).on("change", "#checkbox-stock", function(){
        if ($("#checkbox-stock").is(":checked")){
            gInstock = 0;
            gCurrentPage = 1;
            callApiFilterProduct(0,12);
        }
        if ($("#checkbox-stock").is(":checked") == false){
            gInstock = -1;
            gCurrentPage = 1;
            callApiFilterProduct(0,12);
        }
        
    })

    // Tạo sự kiện nhấn nút đăng xuất tk
    $(document).on("click", "#btn-signout", function(){
        redirectToLogin();
        window.location.href = "./login.html"
    })
    // Tạo sự kiện khi nhấn icon user
    $(document).on("click", "#icon-user", function(){
        $("#user-info").removeClass("d-none");
        $("#icon-user").addClass("open");
    })
    // ạo sự kiện nhấn nút đóng icon user
    $(document).on("click", "#icon-user.open", function(){
        $("#user-info").addClass("d-none");
        $("#icon-user").removeClass("open");
    })
    // Thêm sản phẩm vào giỏ hàng
    $(document).on("click", "#btn-add", function(){
        onBtnAddToCartClick(this);
    })
    // Thêm sự kiện chọn hãng tìm kiếm
    $(document).on("click", "#apple-brand", function(){
        gBrandSelected = "APPLE";
        $(".block-brands .brand-item").removeClass("active");
        $("#apple-brand").addClass("active");
        gCurrentPage = 1;
        callApiFilterProduct(0,12);
    })
    $(document).on("click", "#jbl-brand", function(){
        gBrandSelected = "JBL";
        $(".block-brands .brand-item").removeClass("active");
        $("#jbl-brand").addClass("active");
        gCurrentPage = 1;
        callApiFilterProduct(0,12);
    })
    $(document).on("click", "#samsung-brand", function(){
        gBrandSelected = "SAMSUNG";
        $(".block-brands .brand-item").removeClass("active");
        $("#samsung-brand").addClass("active");
        gCurrentPage = 1;
        callApiFilterProduct(0,12);
    })
    $(document).on("click", "#sony-brand", function(){
        gBrandSelected = "SONY";
        $(".block-brands .brand-item").removeClass("active");
        $("#sony-brand").addClass("active");
        gCurrentPage = 1;
        callApiFilterProduct(0,12);
    })
    $(document).on("click", "#lg-brand", function(){
        gBrandSelected = "LG";
        $(".block-brands .brand-item").removeClass("active");
        $("#lg-brand").addClass("active");
        gCurrentPage = 1;
        callApiFilterProduct(0,12);
    })
    $(document).on("click", "#benq-brand", function(){
        gBrandSelected = "BENQ";
        $(".block-brands .brand-item").removeClass("active");
        $("#benq-brand").addClass("active");
        gCurrentPage = 1;
        callApiFilterProduct(0,12);
    })
    $(document).on("click", "#all-brand", function(){
        gBrandSelected = "all";
        $(".block-brands .brand-item").removeClass("active");
        $("#all-brand").addClass("active");
        gCurrentPage = 1;
        callApiFilterProduct(0,12);
    })

    // Tạo sự kiện nhấn chọn categories
    $(document).on("click", "#head-phone", function(){
        gCategories = 1;
        $(".list-categories li").removeClass("active");
        $("#head-phone").addClass("active");
        gCurrentPage = 1;
        callApiFilterProduct(0,12);
    })
    $(document).on("click", "#laptop", function(){
        gCategories = 2;
        $(".list-categories li").removeClass("active");
        $("#laptop").addClass("active");
        gCurrentPage = 1;
        callApiFilterProduct(0,12);
    })
    $(document).on("click", "#tivi", function(){
        gCategories = 3;
        $(".list-categories li").removeClass("active");
        $("#tivi").addClass("active");
        gCurrentPage = 1;
        callApiFilterProduct(0,12);
    })
    $(document).on("click", "#speaker", function(){
        gCategories = 4;
        $(".list-categories li").removeClass("active");
        $("#speaker").addClass("active");
        gCurrentPage = 1;
        callApiFilterProduct(0,12);
    })
    $(document).on("click", "#camera", function(){
        gCategories = 5;
        $(".list-categories li").removeClass("active");
        $("#camera").addClass("active");
        gCurrentPage = 1;
        callApiFilterProduct(0,12);
    })
    $(document).on("click", "#game", function(){
        gCategories = 6;
        $(".list-categories li").removeClass("active");
        $("#game").addClass("active");
        gCurrentPage = 1;
        callApiFilterProduct(0,12);
    })
    $(document).on("click", "#all-categories", function(){
        gCategories = "all";
        $(".list-categories li").removeClass("active");
        $("#all-categories").addClass("active");
        gCurrentPage = 1;
        callApiFilterProduct(0,12);
    })
    $(document).on("click", "#apply-price-btn", function(){
        gCurrentPage = 1;
        callApiFilterProduct(0,12);
    })

    // Tạo sự kiện chuyển trang
    $(document).on("click", "#next-page", function(){
        callApiFilterProduct(gCurrentPage,12);
        gCurrentPage += 1;
    })
    // Tạo sự kiện chuyển trang
    $(document).on("click", "#prev-page", function(){
        if(gCurrentPage > 1){
            callApiFilterProduct((gCurrentPage - 2),12);
            gCurrentPage -= 1;
        }
    })
    // Tạo sự kiện nhấn icon thêm sản phẩm vào danh sách yêu thích
    $(document).on("click", ".unfavourite-icon", function(){
        onBtnAddWishListClick(this);
    })
    // Tạo sự kiện nhấn icon bỏ sản phẩm yêu thích
    $(document).on("click", ".favourite-icon", function(){
        onBtnRemoveWishListClick(this);
    })
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading(){
        "use strict"
        callApiCheckUser();

        callApiGetAllproducts(0,20);

        callApiGetAllOffice();
    }

    // Hàm xử lí thêm sản phẩm yêu thích
    function onBtnAddWishListClick(paramIcon){
        "use strict"
        var vProductId = $(paramIcon).data("id");
        if(gUser != -1){
            callApiCreateWishItem(vProductId);
        }
    }
    // Hàm xử lí bỏ sản phẩm yêu thích
    function onBtnRemoveWishListClick(paramIcon){
        "use strict"
        var vProductId = $(paramIcon).data("id");
        if(gUser != -1){
            callApiDeleteWishItem(vProductId);
        }
    }
    // Hàm thêm sản phẩm vào giỏ hàng
    function onBtnAddToCartClick(paramE){
        "use strict"
        
        var vProductObj = {
            id: "",
            quantity_order: 1,
            priceEach: ""
        }
        
        readProductData(vProductObj,paramE);
        var bIndex = 0;
        var vFound = false;

        if (gProductListId == null){
            gProductListId = [];
        }
        while(vFound == false &&  bIndex < gProductListId.length){
            if(gProductListId[bIndex].id == vProductObj.id){
                gProductListId[bIndex].quantity_order = gProductListId[bIndex].quantity_order + 1;
                vFound = true;
            }
            else {
                bIndex ++;
            }
        }
         if (vFound == false){
            gProductListId.push(vProductObj);
         }

        gNumberProduct.html(gProductListId.length)

        localStorage.setItem("productList",JSON.stringify(gProductListId));
    }

    // Hàm kiểm tra người dùng đăng nhập
    function callApiCheckUser(){
        $.ajax({
            url: urlInfo,
            type: "GET",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
                },
            success: function(responseObject) {
                if(gUser != responseObject.id){
                    localStorage.clear();
                    localStorage.setItem("user", responseObject.id);
                    gProductListId = [];
                }
                responseHandler();
            },
            error: function(xhr) {
                redirectToLogin();
                if(gUser != "-1"){
                    localStorage.clear();
                    localStorage.setItem("user", "-1");
                    gProductListId = [];
                }
                if (gProductListId != null){
                    gNumberProduct.html(gProductListId.length);
                }
            }
        });
    }

    // Hàm gọi api lấy sản phẩm qua tên sản phẩm
    function callApiSearchProductByName(paramName,paramPage,paramSize){
        "use strict"
        $.ajax({
            url: gUrlProductByProductName + paramName
            + "?page=" + paramPage + "&size=" + paramSize,
            type: "GET",
            dataType: "json",
            success: function(responseObject) {
                loadProductToPage(responseObject);
            },
            error: function(er) {
                errorHandler();
            }
        });
    }
    // Hàm gọi api lấy toàn bộ sản phẩm
    function callApiGetAllproducts(paramPage, paramSize){
        $.ajax({
            url: gUrlProduct + "?page=" + paramPage + "&size=" + paramSize,
            type: "GET",
            dataType: "json",
            success: function(responseObject) {
                loadProductToPage(responseObject);
            },
            error: function(er) {
                errorHandler();
            }
        });
    }

    // Hàm gọi api lấy toàn bộ danh sách yêu thích
    function callApiGetWishList(){
        "use strict"
        $.ajax({
            url: gUrlWishlist,
            type: "GET",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
                },
            success: function(responseObject) {
                wishListHandler(responseObject);
            },
            error: function() {
            }
        });
    }
    // Hàm gọi api lấy toàn bộ sản phẩm
    function callApiFilterProduct(paramPage, paramSize){
        $.ajax({
            url: gUrlFilterProduct 
            + "/product-vendor/" + gBrandSelected
            + "/product-line-id/" + gCategories
            + "/price1/" + gPrice1
            + "/price2/" + gPrice2
            + "/quantity-in-stock/" + gInstock
            + "/onsale/" + gOnSale
            + "/product-name/" + gSearch
            + "?page=" + paramPage + "&size=" + paramSize,
            type: "GET",
            dataType: "json",
            success: function(responseObject) {
                loadProductToPage(responseObject);
            },
            error: function(er) {
                errorHandler();
            }
        });
    }
    // Hàm thêm sản phẩm yêu thích cho người dùng
    function callApiCreateWishItem(paramProductId){
        "use strict"
        $.ajax({
            url: gUrlWishlist + "?productId=" + paramProductId,
            type: "POST",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
                },
            success: function() {
                $("#product-wish-"+ paramProductId +"").html(`
                <i data-id = "`+ paramProductId +`" class="fa-solid fa-heart favourite-icon"></i>
                `)
            },
            error: function() {
            }
        });
    }
    // Hàm xoá sản phẩm yêu thích cho người dùng
    function callApiDeleteWishItem(paramProductId){
        "use strict"
        $.ajax({
            url: gUrlWishlist + "?productId=" + paramProductId,
            type: "DELETE",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
                },
            success: function() {
                $("#product-wish-"+ paramProductId +"").html(`
                <i data-id = "`+ paramProductId +`" class="fa-regular fa-heart unfavourite-icon"></i>
                `)
            },
            error: function() {

            }
        });
    }
    // Hàm gọi api lấy toàn bộ office
    function callApiGetAllOffice(){
        "use strict"
        $.ajax({
            url: gUrlOffice,
            type: "GET",
            dataType: "json",
            success: function(responseObject){
                loadOfficeToPage(responseObject);
            },
            error: function() {
            }
        });
    }
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // Hàm hiển thị dữ liệu sản phẩm lên trang
    function loadProductToPage(paramRes){
        "use strict"

        var vProductContain = $("#list-product");
        vProductContain.empty();

        setPage(paramRes.length);

        if(paramRes.length > 11){
            $(".pagination").removeClass("d-none");
        }
        else if(gCurrentPage == 1 && paramRes.length < 12) {
            $(".pagination").addClass("d-none");
        }

        // Giới hạn độ dài sản phẩm
        var vLength = paramRes.length;
        if(vLength > 11){
            vLength = 12
        }
        for(var bIndex = 0; bIndex < vLength; bIndex ++){
            var bDisable = "";
            if (paramRes[bIndex].quantityInStock < 1){
                bDisable = "disabled";
            }
            var vPrice = `<p class="price"><span class="discount">$` + paramRes[bIndex].buyPrice + `</span> $` + paramRes[bIndex].discountPrice + `</p>`

            if(paramRes[bIndex].discountPrice >= paramRes[bIndex].buyPrice ){
                vPrice = `<p class="price text-dark">$` + paramRes[bIndex].discountPrice + `</p>`
            }
            var bProduct = `<div class="col-6 col-lg-3 col-md-4 col-sm-4">
                <div class="item">
                    <a href="./product-details.html?id=` + paramRes[bIndex].id + `">
                        <img src="` + gUrlDownloadPhoto + "/" + paramRes[bIndex].photo1 + `" alt="` + paramRes[bIndex].productName + `"/>
                    </a>
                    <p class="product-name line-clamp-1">` + paramRes[bIndex].productName + `</p>
                    ` + vPrice +`
                    <p id = "stock-id-` + paramRes[bIndex].id + `" class="stock">Quantity in stock: ` + paramRes[bIndex].quantityInStock + ` </p>
                    <div class="cart-block">
                        <button  `+ bDisable +` id="btn-add" data-price = "` + paramRes[bIndex].discountPrice 
                        + `" data-id = "` + paramRes[bIndex].id + `" class="btn-add-cart"><i class="fa-solid fa-cart-plus"></i></button>
                        <div class="wish-list-block" id = "product-wish-`+ paramRes[bIndex].id +`">
                            <i data-id = "`+ paramRes[bIndex].id +`" class="fa-regular fa-heart unfavourite-icon"></i>
                        </div>
                    </div>
                </div>
            </div>`

        vProductContain.append(bProduct);
        }
        // Lấy danh sách wish list
        callApiGetWishList();

        if(paramRes.length == 0){
            errorHandler();
        }
        gSrTop.reveal(".item",{interval: 100})
    }

    // Hàm hiển thị thông tin văn phòng
    function loadOfficeToPage(paramRes){
        "use strict"
        var vOfficeContain = $("#store-list");
        vOfficeContain.empty();

        for(let bIndex = 0; bIndex < paramRes.length; bIndex++){
            let bOfficeItem = `
            <div class="col-12 col-sm-6 col-md-3 mt-5">
                <div class="store-item">
                    <p class="store-title"><i class="fa-solid fa-store"></i> Stores - `+ (bIndex + 1) +`</p>
                    <p class="address"><i class="fa-solid fa-location-dot"></i> ` + paramRes[bIndex].address_line + ", " + paramRes[bIndex].city + `</p>
                    <p class="phone"><i class="fa-solid fa-phone-volume"></i> `+ paramRes[bIndex].phone +`</p>
                </div>
            </div>`

            vOfficeContain.append(bOfficeItem);
        }
        gSrTop.reveal(".store-item", {interval: 200});
    }
    
    // Hàm xử lí phân trang
    function setPage(paramLength){
        "use strict"
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
        var vPageContainer = $(".pagination");
        vPageContainer.empty();
        var vPageItem = `
            <li id="prev-page" class="page-item actives"><a class="page-link" href="#!">Previous</a></li>
            <li id="page-`+ gCurrentPage +`" class="page-item actives"><a class="page-link" href="#!">`+ gCurrentPage +`</a></li>
            <li id="next-page" class="page-item"><a class="page-link" href="#!">Next</a></li>`
        vPageContainer.append(vPageItem);

        if(paramLength < 12){
            $("#next-page").prop("disabled", true);
        }
        else {
            $("#next-page").prop("disabled", false);
        }

        if(paramLength < 12 && gCurrentPage == 1){
            $("#prev-page").prop("disabled", true);
        }
        else {
            $("#prev-page").prop("disabled", false);
        }
        
    }
    //Hàm thu thập dữ liệu khi nhấn thêm sản phẩm
    function readProductData(paramObj, paramE){
        "use strict"
        paramObj.id = $(paramE).attr("data-id");
        paramObj.priceEach = $(paramE).attr("data-price");
    }
    // Hàm xử lí danh sách wishList;
    function wishListHandler(paramRes){
        "use strict"
        paramRes.forEach(wishItem => {
            $("#product-wish-"+ wishItem.product.id +"").html(`
            <i data-id = "`+ wishItem.product.id +`" class="fa-solid fa-heart favourite-icon"></i>
            `)
        });
    }

    //Hàm get Cookie đã giới thiệu ở bài trước
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    // Hàm xử lí sau khi đăng nhập
    function responseHandler(){
        "use strict"
        $("#icon-login").addClass("d-none");
        $("#icon-user").removeClass("d-none");
        if(gProductListId != null){
            gNumberProduct.html(gProductListId.length);
        }
    }

    //Hàm logout
    function redirectToLogin() {
        // Trước khi logout cần xóa token đã lưu trong cookie
        setCookie("token", "", 1);
    }
    //Hàm setCookie
    function setCookie(cname, cvalue) {
        document.cookie = cname + "=" + cvalue + ";" + ";path=/";
    }
    
    // Hàm xử lí khi không tìm thấy sản phẩm
    function errorHandler(){
        "use strict"
        var vProductContain = $("#list-product");
        vProductContain.empty();
        vProductContain.html(`
        <div class="col-12 mt-5">
            <h2 class="text-danger text-center">NO PRODUCTS FOUND!</h2>
        </div>`);
        $(".pagination").addClass("d-none");
    }
    // Hàm xử lí tìm kiếm
    function onInpSearchChange(){
        "use strict"
        var vSearch = $("#inp-search").val().trim();
        if(vSearch != ""){
            callApiSearchProductByName(vSearch,0,12);
        }
    }
});