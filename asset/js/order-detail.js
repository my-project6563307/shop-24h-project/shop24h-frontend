$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    // Khai báo url
    var gUrlProducts = "http://localhost:8080/shop24h/api/noauth/products?page=0&size=50";

    var urlInfo = "http://localhost:8080/shop24h/api/noauth/customers";

    // Lấy token
    const vtoken = getCookie("token");

    // Cấu hình data table
    var vDataTable = $('#product-table').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            columns: [
                {data: "id"},
                {data: "action"},
                {data: "photo1"},
                {data: "productName"},
                {data: "productVendor"},
                {data: "quantityInStock"},
                {data: "buyPrice"},
                {data: "discountPrice"},
                {data: "productLine.productLine"},

            ],
            columnDefs: [
                {
                targets: 1,
                defaultContent: '<i id = "delete-address-map" class="fas fa-trash-alt delete-icon" style ="color:red;"></i>'
                },
                {
                targets: 2,
                render: function(data){
                    return `<img class="table-img" src="`+ data +`">`
                }
                }
              ]
    });
    // Lưu id sản phẩm dc chọn
    var gIdProduct = ""; 
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading();

    // Tạo sự kiện nhấn dòng trên bảng
    $(document).on("click", "#product-table td", function(){
        onBtnRowClick(this);
    })

    // Tạo sự kiến nhấn nút thêm products
    $(document).on("click", "#btn-add", function(){
        onBtnAddClick();
    })

    // Tạo sự kiến nhấn nút cập nhật products
    $(document).on("click", "#btn-add-product", function(){
        onBtnAddProductClick();
    })
    // Tạo sự kiến nhấn nút xoá products
    $(document).on("click", "#product-table tr .delete-icon", function(){
        onIconDeleteRowClick(this);
    })
    // Tạo sự kiến nhấn nút cập nhật products
    $(document).on("click", "#btn-update", function(){
        onBtnUpdateProductClick();
    })
    // Tạo sự kiến nhấn xác nhận xoá products
    $(document).on("click", "#btn-delete", function(){
        onBtnDeleteProductClick();
    })
    
    // Tạo sự kiện tải ảnh sản phẩm
    $(document).on("change", "#inp-photo-file", function(){
        uploadImage();
        $("#product-img").removeClass("d-none");
    })
    // Tạo sự kiện tải ảnh sản phẩm
    $(document).on("change", "#inp-photo-url", function(){
        $("#product-img").attr("src", $("#inp-photo-url").val().trim());
        if($("#inp-photo-url").val().trim() != ""){
            $("#product-img").removeClass("d-none");
        }
        else {
            $("#product-img").addClass("d-none");
        }
    })
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading(){
        "use strict"
        callApiCheckUser();
        
    }
    // Hàm xử lí nhấn nút đổi hình ảnh
    function uploadImage() {
        var fileInput = document.getElementById("inp-photo-file");
        const previewImage = document.getElementById('product-img');

        const selectedFile = fileInput.files[0];
        const reader = new FileReader();

        if (selectedFile) {
            reader.readAsDataURL(selectedFile);
        }
        reader.addEventListener('load', () => {
            previewImage.src = reader.result;
        });
    }
    // Hàm gọi api lấy toàn bộ sản phẩm
    function callApiGetAllproducts(){
        $.ajax({
            url: gUrlProducts,
            type: "GET",
            dataType: "json",
            success: function(responseObject) {
                loadProductToTable(responseObject);
            },
            error: function(er) {
                // errorHandler();
            }
        });
    }
    // Hàm tạo mới sản phẩm
    function callApiCreateProduct(paramObj){
        "use strict"
        var vUrl = "http://localhost:8080/shop24h/products";
        var vJsonObj = JSON.stringify(paramObj);
        $.ajax({
            url: vUrl + "?productLineId=" + paramObj.productLineId,
            type: "POST",
            contentType: "application/json",
            data: vJsonObj,
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function() {
                toastr.success('Create a new product successully');
                callApiGetAllproducts();
            },
            error: function() {
                toastr.error('Error! Please try again.');
            }
        });
    }
    // Hàm cập nhật sản phẩm
    function callApiUpdateProduct(paramObj){
        "use strict"
        var vUrl = "http://localhost:8080/shop24h/products/" + gIdProduct;
        var vJsonObj = JSON.stringify(paramObj);
        $.ajax({
            url: vUrl + "?productLineId=" + paramObj.productLineId,
            type: "PUT",
            contentType: "application/json",
            data: vJsonObj,
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function() {
                toastr.success('Update the product successully');
                callApiGetAllproducts();
            },
            error: function() {
                toastr.error('Error! Please try again.');
            }
        });
    }
    // Hàm xoá sản phẩm
    function callApiDeleteProduct(){
        "use strict"
        var vUrl = "http://localhost:8080/shop24h/products/" + gIdProduct;
        $.ajax({
            url: vUrl,
            type: "DELETE",
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function() {
                toastr.success('Delete the product successully');
                callApiGetAllproducts();
            },
            error: function() {
                toastr.error('Error! Please try again.');
            }
        });
    }
    // Hàm cập nhật sản phẩm
    function onBtnUpdateProductClick(){
        "use strict"
        var vProductObj = {
            productCode: "",
            productName: "",
            buyPrice: "",
            discountPrice: "",
            productVendor: "",
            quantityInStock: "",
            productDescription:"",
            productLineId: "",
            productDescription: "",
            photo1: "",
            display: ""
        }
        // Thu thập dữ liệu
        readFormData(vProductObj);
        // Xử lí dữ liệu
        var vValid = validateData(vProductObj);
        if(vValid){
            callApiUpdateProduct(vProductObj);
        }
    }
    // Hàm xoá sản phẩm
    function onBtnDeleteProductClick(){
        "use strict"
        $("#modal-delete").modal("hide");
        callApiDeleteProduct();
    }
    // Hàm kiểm tra đăng nhập
    function callApiCheckUser(){
        $.ajax({
            url: urlInfo,
            type: "GET",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function(responseObject) {
                if(responseObject.roles[0].name == "ROLE_MANAGER"){
                    callApiGetAllproducts();
                    responseHandler(responseObject);
                }
                else{
                    redirectToLogin();
                }
            },
            error: function(xhr) {
                // Khi token hết hạn, AJAX sẽ trả về lỗi khi đó sẽ redirect về trang login để người dùng đăng nhập lại
                redirectToLogin();
            }
        });
    }
    // Hàm xử lí khi nhấn row
    function onBtnRowClick(paramRow){
        "use strict"
        gIdProduct = "";
        var vDataRow = onRowClick(paramRow);
        if(vDataRow.length > 0){
            loadDataToForm(vDataRow);
        }
    }

    // Hàm xử lí nhấn xoá sản phẩm
    function onIconDeleteRowClick(paramIcon){
        gIdProduct = "";
        var vDataRow = onRowClick(paramIcon);
        gIdProduct = vDataRow[0].id;
        $("#modal-delete").modal("show");
    }
    // Hàm xử lí khi nhấn thêm product
    function onBtnAddClick(){
        "use strict"
        emptyForm();
    }
    // Hàm xử lí khi nhấn thêm sản phẩm
    function onBtnAddProductClick(){
        "use strict"
        var vProductObj = {
            productCode: "",
            productName: "",
            buyPrice: "",
            discountPrice: "",
            productVendor: "",
            quantityInStock: "",
            productDescription:"",
            productLineId: "",
            productDescription: "",
            photo1: "",
            display: ""
        }
        // Thu thập dữ liệu
        readFormData(vProductObj);
        // Xử lí dữ liệu
        var vValid = validateData(vProductObj);
        if(vValid){
            callApiCreateProduct(vProductObj);
        }
    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // Hàm thu thập dữ liệu tạo mới product
    function readFormData(paramObj){
        "use strict"
        paramObj.productCode = $("#inp-product-code").val();
        paramObj.productName = $("#inp-product-name").val();
        paramObj.quantityInStock = $("#inp-quantity").val();
        paramObj.productVendor = $("#select-product-vendor").val();
        paramObj.productLineId = $("#select-product-line").val();
        paramObj.display = $("#select-display").val();
        paramObj.buyPrice = $("#inp-buy-price").val();
        paramObj.discountPrice = $("#inp-discount-price").val();
        paramObj.productDescription = $("#inp-desc").val();
        paramObj.photo1 = $("#product-img").attr("src");
    }
    // Hàm xử lí dữ liệu
    function validateData(paramObj){
        "use strict"
        if(paramObj.productCode == ""){
            toastr.error('Please fill out product code');
            $("#inp-product-code").focus();
            return false;
        }
        if(paramObj.productName == ""){
            toastr.error('Please fill out product name');
            $("#inp-product-name").focus();
            return false;
        }
        if(paramObj.buyPrice == ""){
            toastr.error('Please fill out buy price');
            $("#inp-buy-price").focus();
            return false;
        }
        if(paramObj.discountPrice == ""){
            toastr.error('Please fill out discount price');
            $("#inp-discount-price").focus();
            return false;
        }
        if(paramObj.productVendor == ""){
            toastr.error('Please choose a product vendor');
            $("#select-product-vendor").focus();
            return false;
        }
        if(paramObj.display == ""){
            toastr.error('Please choose display status');
            $("#select-display").focus();
            return false;
        }
        if(paramObj.quantityInStock == ""){
            toastr.error('Please fill out quantity');
            $("#inp-quantity").focus();
            return false;
        }
        if(paramObj.productDescription == ""){
            toastr.error('Please fill out description');
            $("#inp-desc").focus();
            return false;
        }
        if(paramObj.productLineId == ""){
            toastr.error('Please choose a producline');
            $("#select-product-line").focus();
            return false;
        }
        if(paramObj.productLineId == ""){
            toastr.error('Please choose a producline');
            $("#select-product-line").focus();
            return false;
        }
        if(paramObj.photo1 == ""){
            toastr.error('Please choose a photo');
            return false;
        }

    return true;
    }
    //Hàm logout
    function redirectToLogin() {
        // Trước khi logout cần xóa token đã lưu trong cookie
        setCookie("token", "", 1);
        window.location.href = "./login.html";
    }
    //Hàm setCookie
    function setCookie(cname, cvalue) {
        document.cookie = cname + "=" + cvalue + ";" + ";path=/";
    }
    //Hàm get Cookie đã giới thiệu ở bài trước
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
    // Hàm xử lí sau kiểm tra người dùng thành công
    function responseHandler(paramRes){
        "use strict"
        $("#username").html(paramRes.username);
        $("#user-img").attr("src", paramRes.photo);
    }
    // Hàm ghi dữ liệu vào bảng
    function loadProductToTable(paramRes){
        "use strict"
        vDataTable.clear();
        vDataTable.rows.add(paramRes);
        vDataTable.draw();
    }

    // Hàm lấy dữ liệu khi chọn row
    function onRowClick(paramRow){
        "use strict"
        var vRow = $(paramRow).closest("tr");
        $("#product-table tr").removeClass("active");
        vRow.addClass("active");
        var vDataOnRow = vDataTable.rows(vRow).data();
        return vDataOnRow;
    }
    // Hàm tải dữ liệu lên form
    function loadDataToForm(paramRowData){
        "use strict"
        $("#btn-action").html(`<button id="btn-update" class="btn btn-primary">Update</button>`);
        gIdProduct = paramRowData[0].id;
        $("#inp-id").removeClass("d-none");
        $("#label-id").removeClass("d-none");
        $("#inp-id").val(paramRowData[0].id);
        $("#inp-product-code").val(paramRowData[0].productCode);
        $("#inp-product-name").val(paramRowData[0].productName);
        $("#inp-buy-price").val(paramRowData[0].buyPrice);
        $("#inp-discount-price").val(paramRowData[0].discountPrice);
        $("#inp-quantity").val(paramRowData[0].quantityInStock);
        $("#inp-desc").val(paramRowData[0].productDescription);
        $("#select-product-vendor").val(paramRowData[0].productVendor);
        $("#select-product-line").val(paramRowData[0].productLine.id);
        $("#select-display").val(paramRowData[0].display);
        $("#product-img").attr("src", paramRowData[0].photo1)
        $("#product-img").removeClass("d-none");
    }
    // Hàm xoá dữ liệu trên form
    function emptyForm(){
        "use strict"
        $("#btn-action").html(`<button id="btn-add-product" class="btn btn-info">Add</button>`);
        gIdProduct = "";
        $("#inp-id").val("");
        $("#inp-id").addClass("d-none");
        $("#label-id").addClass("d-none");
        $("#inp-product-code").val("");
        $("#inp-product-name").val("");
        $("#inp-quantity").val("");
        $("#select-product-vendor").val("");
        $("#select-product-line").val("");
        $("#select-display").val("");
        $("#product-img").attr("src", "")
        $("#product-img").addClass("d-none");
        $("#inp-buy-price").val("");
        $("#inp-discount-price").val("");
        $("#inp-desc").val("");
        $("#product-img").attr("src", "")
    }
})