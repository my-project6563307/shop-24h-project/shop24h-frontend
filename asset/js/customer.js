$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    // Khai báo url
    var gUrlCustomer = "http://localhost:8080/shop24h/customers";

    var urlInfo = "http://localhost:8080/shop24h/api/noauth/customers";

    var gUrlUploadPhoto = "http://localhost:8080/shop24h/api/noauth/uploadFile";

    var gUrlDownloadPhoto = "http://localhost:8080/shop24h/api/noauth/downloadFile";

    var gExcelExportUrl = "http://localhost:8080/shop24h/api/noauth/export/customer/excel";

    // Lấy token
    const vtoken = getCookie("token");

    // Cấu hình data table
    var vDataTable = $('#customer-table').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            columns: [
                {data: "id"},
                {data: "active"},
                {data: "firstName"},
                {data: "lastName"},
                {data: "email"},
                {data: "photo"},
                {data: "phoneNumber"},
                {data: "address"},
                {data: "city"},
                {data: "username"}
            ],
            columnDefs: [
                {
                    targets: 1,
                    render: function(data){
                        if(data != null && data == true){
                            return `<i class="fas fa-check-circle text-success"></i>`
                        }
                        else if(data != null && data == false){
                            return `<i class="fas fa-ban text-danger"></i>`
                        }
                        else {
                            return "";
                        }
                    }
                },
                {
                    targets: 5,
                    render: function(data){
                        if(data != null){
                            return `<img class="table-img" src="`+ gUrlDownloadPhoto +"/" + data +`">`
                        }
                        return "";
                    }
                }
            ]
    });
    // Lưu id sản phẩm dc chọn
    var gIdCustomer = ""; 
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading();

    // Tạo sự kiện logout
    $(document).on("click", "#btn-logout", function(){
        redirectToLogin();
      })
    // Tạo sự kiện nhấn dòng trên bảng
    $(document).on("click", "#customer-table td", function(){
        onBtnRowClick(this);
        $("#myModal").modal("show");
    })
    // Tạo sự kiến nhấn nút thêm customer
    $(document).on("click", "#btn-add", function(){
        onBtnAddClick();
        $("#myModal").modal("show");
    })
    // Tạo sự kiến nhấn nút cập nhật customer
    $(document).on("click", "#btn-add-customer", function(){
        onBtnAddCustomerClick();
    })
    // Tạo sự kiến nhấn nút cập nhật customer
    $(document).on("click", "#btn-update", function(){
        onBtnUpdateCustomerClick();
    })
    // Tạo sự kiện tải ảnh sản phẩm
    $(document).on("change", "#inp-photo-file", function(){
        uploadImage();
        $("#customer-img").removeClass("d-none");
    })
    // Tạo sự kiện tải ảnh sản phẩm
    $(document).on("change", "#inp-photo-url", function(){
        $("#customer-img").attr("src", $("#inp-photo-url").val().trim());
        if($("#inp-photo-url").val().trim() != ""){
            $("#customer-img").removeClass("d-none");
        }
        else {
            $("#customer-img").addClass("d-none");
        }
    })
    // Tạo sự kiện xuất file excel
    $(document).on("click", "#btn-export", function(){
        callApigetexcel();
    })
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading(){
        "use strict"
        callApiCheckUser();
    }
    // Hàm xử lí nhấn nút đổi hình ảnh
    function uploadImage() {
        var fileInput = document.getElementById("inp-photo-file");
        const previewImage = document.getElementById('customer-img');

        const selectedFile = fileInput.files[0];
        const reader = new FileReader();
        
        $("#inp-photo-name").val(selectedFile.name);

        if (selectedFile) {
            reader.readAsDataURL(selectedFile);
        }
        reader.addEventListener('load', () => {
            previewImage.src = reader.result;
        });
    }
    // Hàm gọi api lấy toàn bộ customer
    function callApiGetAllCustomer(){
        $.ajax({
            url: gUrlCustomer + "?page=0" + "&size=30",
            type: "GET",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function(responseObject) {
                toastr.success("Get customers successfully");
                loadProductToTable(responseObject);
            },
            error: function(er) {
                toastr.error("Error! fail to get data");
            }
        });
    }
    // Hàm gọi api xuất excel
    function callApigetexcel(){
        "use strict"
        var vPayment1 = "";
        var vPayment2 = "";
        var gLevelSelected = $("#select-level").val();
        if(gLevelSelected == "Platinum"){
            vPayment1 = "5000";
            vPayment2 = "9999999999999999";
        }
        else if(gLevelSelected == "Gold"){
            vPayment1 = "2000";
            vPayment2 = "4999.99";
        }
        else if(gLevelSelected == "Silver"){
            vPayment1 = "1000";
            vPayment2 = "1999.99";
        }
        else if(gLevelSelected == "Vip"){
            vPayment1 = "500";
            vPayment2 = "999.99";
        }
        $.ajax({
        url: gExcelExportUrl
        + "?payment1=" + vPayment1 + "&payment2=" + vPayment2,
        type: "GET",
        "contentType": "application/octet-stream",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
            },
            success: function() {
            window.location.href = gExcelExportUrl
            + "?payment1=" + vPayment1 + "&payment2=" + vPayment2;
            },
            error: function(er) {
            toastr.error("Export file error");
            }
        });
    }
    // Hàm tạo mới customer
    function callApiCreateCustomer(paramObj){
        "use strict"
        var vUrl = "http://localhost:8080/shop24h/customers";
        var vJsonObj = JSON.stringify(paramObj);
        $.ajax({
            url: vUrl + "?roleId=" + paramObj.roleId,
            type: "POST",
            contentType: "application/json",
            data: vJsonObj,
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function() {
                toastr.success('Create a new customer successully');
                callApiGetAllCustomer();
            },
            error: function(e) {
                toastr.error('Error! Please try again.');
            }
        });
    }
    // Hàm cập nhật customer
    function callApiUpdateCustomer(paramObj){
        "use strict"
        var vUrl = "http://localhost:8080/shop24h/customers/" + gIdCustomer;

        var vJsonObj = JSON.stringify(paramObj);
        $.ajax({
            url: vUrl + "?roleId=" + paramObj.roleId,
            type: "PUT",
            contentType: "application/json",
            data: vJsonObj,
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function() {
                toastr.success('Update the customer successully');
                callApiGetAllCustomer();
            },
            error: function() {
                toastr.error('Error! Please try again.');
            }
        });
    }
    // Hàm cập nhật customer
    function onBtnUpdateCustomerClick(){
        "use strict"
        var vAddObj = {
            username: "",
            password: "",
            firstName: "",
            lastName: "",
            phoneNumber: "",
            address: "",
            city: "",
            email: "",
            photo: "",
            roleId: "",
            active: "",
        }
        // Thu thập dữ liệu
        readFormData(vAddObj);
        // Xử lí dữ liệu
        var vValid = validateData(vAddObj);
        if(vValid && $("#inp-photo-file").val() == ""){
            callApiUpdateCustomer(vAddObj);
        }
        else if (vValid && $("#inp-photo-file").val() != ""){
            callApiUploadPhoto();
            callApiUpdateCustomer(vAddObj);
        }
    }
    // Hàm upload hình ảnh
    function callApiUploadPhoto(){
        "use strict"
        var fileInput = document.getElementById("inp-photo-file");
        var selectedFile = fileInput.files[0];
        var form = new FormData();
        form.append("file", selectedFile, selectedFile.name);

        $.ajax({
            "url": gUrlUploadPhoto,
            "method": "POST",
            "timeout": 0,
            "processData": false,
            "mimeType": "multipart/form-data",
            "contentType": false,
            "data": form,
            async: false,
            success: function(res) {
                toastr.success('Upload photo successfully');
            },
            error: function() {
                toastr.error('Error! Please try again.');
            }
        });
    }
    // Hàm kiểm tra đăng nhập
    function callApiCheckUser(){
        $.ajax({
            url: urlInfo,
            type: "GET",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function(responseObject) {
                if(responseObject.roles[0].name == "ROLE_MANAGER"){
                    callApiGetAllCustomer();
                    responseHandler(responseObject);
                }
                else{
                    redirectToLogin();
                }
            },
            error: function(xhr) {
                // Khi token hết hạn, AJAX sẽ trả về lỗi khi đó sẽ redirect về trang login để người dùng đăng nhập lại
                redirectToLogin();
            }
        });
    }
    // Hàm xử lí khi nhấn row
    function onBtnRowClick(paramRow){
        "use strict"
        gIdCustomer = "";
        var vDataRow = onRowClick(paramRow);
        if(vDataRow.length > 0){
            loadDataToForm(vDataRow);
        }
    }

    // Hàm xử lí khi nhấn thêm customer
    function onBtnAddClick(){
        "use strict"
        emptyForm();
    }
    // Hàm xử lí khi nhấn thêm customer
    function onBtnAddCustomerClick(){
        "use strict"
        var vAddObj = {
            username: "",
            password: "",
            firstName: "",
            lastName: "",
            phoneNumber: "",
            address: "",
            city: "",
            email: "",
            photo: "",
            roleId: "",
            active: ""
        }
        // Thu thập dữ liệu
        readFormData(vAddObj);
        // Xử lí dữ liệu
        var vValid = validateData(vAddObj);
        if(vValid){
            callApiCreateCustomer(vAddObj);
        }
    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // Hàm thu thập dữ liệu tạo mới 
    function readFormData(paramObj){
        "use strict"
        paramObj.username = $("#inp-username").val();
        paramObj.password = $("#inp-password").val();
        paramObj.firstName = $("#inp-first-name").val();
        paramObj.lastName = $("#inp-last-name").val();
        paramObj.phoneNumber = $("#inp-phone").val();
        paramObj.address = $("#inp-address").val();
        paramObj.city = $("#inp-city").val();
        paramObj.email = $("#inp-email").val();
        paramObj.photo = $("#inp-photo-name").val();
        paramObj.roleId = $("#select-role").val();
        paramObj.active = $("#select-active").val();
    }
    // Hàm xử lí dữ liệu
    function validateData(paramObj){
        "use strict"
        if(paramObj.username == ""){
            toastr.error('Please fill out username');
            $("#inp-username").focus();
            return false;
        }
        if(paramObj.password == ""){
            toastr.error('Please fill out password');
            $("#inp-username").focus();
            return false;
        }
        if(paramObj.password.length < 7){
            toastr.error('Password length must more 7 letters');
            $("#inp-username").focus();
            return false;
        }
        if(paramObj.firstName == ""){
            toastr.error('Please fill out first name');
            $("#inp-first-name").focus();
            return false;
        }
        if(paramObj.lastName == ""){
            toastr.error('Please fill out last name');
            $("#inp-last-name").focus();
            return false;
        }
        if(paramObj.phoneNumber == ""){
            toastr.error('Please fill out phone number');
            $("#inp-phone").focus();
            return false;
        }
        if(paramObj.address == ""){
            toastr.error('Please fill out address');
            $("#inp-address").focus();
            return false;
        }
        if(paramObj.email == ""){
            toastr.error('Please fill out email');
            $("#inp-email").focus();
            return false;
        }
        if(paramObj.photo == ""){
            toastr.error('Please choose a photo');
            return false;
        }

    return true;
    }
    //Hàm logout
    function redirectToLogin() {
        // Trước khi logout cần xóa token đã lưu trong cookie
        setCookie("token", "", 1);
        window.location.href = "./login.html";
    }
    //Hàm setCookie
    function setCookie(cname, cvalue) {
        document.cookie = cname + "=" + cvalue + ";" + ";path=/";
    }
    //Hàm get Cookie đã giới thiệu ở bài trước
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
    // Hàm xử lí sau kiểm tra người dùng thành công
    function responseHandler(paramRes){
        "use strict"
        $("#username").html(paramRes.username);
        $("#user-img").attr("src", gUrlDownloadPhoto +"/"+ paramRes.photo);
    }
    // Hàm ghi dữ liệu vào bảng
    function loadProductToTable(paramRes){
        "use strict"
        vDataTable.clear();
        vDataTable.rows.add(paramRes);
        vDataTable.order( [ 0, 'desc' ] ).draw();
    }

    // Hàm lấy dữ liệu khi chọn row
    function onRowClick(paramRow){
        "use strict"
        var vRow = $(paramRow).closest("tr");
        $("#customer-table tr").removeClass("active");
        vRow.addClass("active");
        var vDataOnRow = vDataTable.rows(vRow).data();
        return vDataOnRow;
    }
    // Hàm tải dữ liệu lên form
    function loadDataToForm(paramRowData){
        "use strict"
        $("#btn-action").html(`<button id="btn-update" class="btn btn-primary">Update</button>`);
        gIdCustomer = paramRowData[0].id;
        $("#inp-id").removeClass("d-none");
        $("#label-id").removeClass("d-none");
        $("#inp-id").val(paramRowData[0].id);

        $("#inp-password").val(paramRowData[0].password);
        $("#inp-username").val(paramRowData[0].username);
        $("#inp-first-name").val(paramRowData[0].firstName);
        $("#inp-last-name").val(paramRowData[0].lastName);
        $("#inp-phone").val(paramRowData[0].phoneNumber);
        $("#inp-address").val(paramRowData[0].address);
        $("#inp-city").val(paramRowData[0].city);
        $("#inp-email").val(paramRowData[0].email);
        $("#inp-photo-name").val(paramRowData[0].photo);
        $("#select-role").val(paramRowData[0].roles[0].id);
        if(paramRowData[0].active == true){
            $("#select-active").val("true");
        }
        else {
            $("#select-active").val("false");
        }
        $("#customer-img").attr("src", gUrlDownloadPhoto + "/" + paramRowData[0].photo);

        if(paramRowData[0].photo != null){
            $("#customer-img").removeClass("d-none");
        }
        else {
            $("#customer-img").addClass("d-none");
        }
    }
    // Hàm xoá dữ liệu trên form
    function emptyForm(){
        "use strict"
        $("#btn-action").html(`<button id="btn-add-customer" class="btn btn-info">Add</button>`);
        gIdCustomer = "";
        $("#inp-id").addClass("d-none");
        $("#label-id").addClass("d-none");
        $("#inp-id").val("");

        $("#inp-password").val("");
        $("#inp-username").val("");
        $("#inp-first-name").val("");
        $("#inp-last-name").val("");
        $("#inp-phone").val("");
        $("#inp-address").val("");
        $("#inp-city").val("");
        $("#inp-email").val("");

        $("#customer-img").attr("src", "")
        $("#customer-img").addClass("d-none");
        $("#select-role").val(1);
        $("#inp-photo-name").val("");
    }
})