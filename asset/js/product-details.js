$(document).ready(function(){
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    const vtoken = getCookie("token");
    var urlInfo = "http://localhost:8080/shop24h/api/noauth/customers";

    var gProductUrl = "http://localhost:8080/shop24h/api/noauth/products/detail/";

    var gRelatedProductUrl = "http://localhost:8080/shop24h/api/noauth/products/product-line/";

    var gUrlWishlist = "http://localhost:8080/shop24h/wishlists";

    var gUrlBoughtProduct = "http://localhost:8080/shop24h/feedbacks"

    var gUrlFeedbacks = "http://localhost:8080/shop24h/api/noauth/feedbacks"

    var gUrlOffice = "http://localhost:8080/shop24h/api/noauth/offices";

    var gUrlDownloadPhoto = "http://localhost:8080/shop24h/api/noauth/downloadFile";

    var gUrl = new URL(window.location.href);

    var gProductId = gUrl.searchParams.get("id");
    
    var gNumberProduct = $("#number-product");
    
    var gQuantity = $("#quantity-product");

    var gUser = localStorage.getItem("user");

    var gProductListId = JSON.parse(localStorage.getItem("productList"));

    var gProductObj = "";

    var gRating = "";
    // Scroll interval
    const gSrTop = ScrollReveal({
        origin: "top",
        distance : "60px",
        duration : 1000,
        reset: false
    })

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading();

    $(document).on("click", "#btn-signout", function(){
        redirectToLogin();
        window.location.href = "./login.html"
    })
    $(document).on("click", "#icon-user", function(){
        $("#user-info").removeClass("d-none");
        $("#icon-user").addClass("open");
    })
    $(document).on("click", "#icon-user.open", function(){
        $("#user-info").addClass("d-none");
        $("#icon-user").removeClass("open");
    })
    // Tạo sự kiện nhấn nút cộng số lượng sản phẩm
    $(document).on("click", "#quantity-plus", function(){
        onIconPlusClick();
    })
    // Tạo sự kiện nhấn nút giảm số lượng sản phẩm
    $(document).on("click", "#quantity-minus", function(){
        onIconMinusClick();
    })
    // Tạo sự kiện nhấn nút thêm sản phẩm vào giỏ hàng
    $(document).on("click", "#btn-add-to-cart", function(){
        if(gProductObj.quantityInStock > 0){
            onBtnAddToCartClick();
        }
    })
    // Thêm sản phẩm vào giỏ hàng
    $(document).on("click", "#btn-add", function(){
        onBtnAddRelatedToCartClick(this);
    })
    // Tạo sự kiện nhấn icon thêm sản phẩm vào danh sách yêu thích
    $(document).on("click", ".unfavourite-icon", function(){
        onBtnAddWishListClick(this);
    })
    // Tạo sự kiện nhấn icon bỏ sản phẩm yêu thích
    $(document).on("click", ".favourite-icon", function(){
        onBtnRemoveWishListClick(this);
    })
    // Tạo sự kiện chọn đánh giá sao
    $(document).on("click", ".block-stars-cmt .fa-star", function(){
        onIconStarClick(this);
    })
    // Tạo sự kiện nhấn nút revew
    $(document).on("click", "#btn-review", function(){
        onBtnReviewClick();
    })
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading(){
        "use strict"
        callApiCheckUser();

        callApiGetProductById();

        callApiGetAllOffice();

        gSrTop.reveal(".path");
    }
    // Hàm xử lí nhấn nút thêm sản phẩm vào giỏ hàng
    function onBtnAddToCartClick(){
        "use strict"
        var vProductObj = {
            id: "",
            quantity_order: 1,
            priceEach: 0
        }
        // thu thập dữ liệu
        readProductDetailsData(vProductObj);
        // Hàm xử lí dữ liệu
        addProductToCart(vProductObj);
    }
    // Hàm xử lí sự kiện nhấn vào icon star
    function onIconStarClick(paramE){
        "use strict"
        gRating =  $(paramE).data("rate");
        $(".block-stars-cmt .fa-star").removeClass("checked")

        for(let i = 1 ; i <= gRating; i++){
            $("#star-"+ i +"").addClass("checked");
        }
    }
    // Hàm xử lí sự kiện nhấn nút tạo review
    function onBtnReviewClick(){
        "use strict"
        var vFeedbackObj = {
            comments: "",
            rating: ""
        }
        // Thu thập dữ liệu
        vFeedbackObj.comments = $("#inp-comments").val().trim();
        vFeedbackObj.rating = gRating;
        // Kiểm tra dữ liệu
        if(vFeedbackObj.rating == ""){
            $("#error").html("You have not choosen a rating!");
        }else{
            $("#error").html("");
            callApiCreateFeedback(gProductId, vFeedbackObj);
        }
    }
    // Hàm xử lí thêm sản phẩm yêu thích
    function onBtnAddWishListClick(paramIcon){
        "use strict"
        var vProductId = $(paramIcon).data("id");
        if(gUser != -1){
            callApiCreateWishItem(vProductId);
        }
    }
    // Hàm xử lí bỏ sản phẩm yêu thích
    function onBtnRemoveWishListClick(paramIcon){
        "use strict"
        var vProductId = $(paramIcon).data("id");
        if(gUser != -1){
            callApiDeleteWishItem(vProductId);
        }
    }
    // Hàm xử lí nhấn nút thêm số lượng
    function onIconPlusClick(){
        "use strict"
        plusQuantity();
    }
    // Hàm xử lí nhấn nút giảm số lượng
    function onIconMinusClick(){
        "use strict"
        minusQuantity();
    }

    // Hàm kiểm tra người dùng đăng nhập
    function callApiCheckUser(){
        $.ajax({
            url: urlInfo,
            type: "GET",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
                },
            success: function(responseObject) {
                if(gUser != responseObject.id){
                    localStorage.clear();
                    localStorage.setItem("user", responseObject.id);
                    gProductListId = [];
                }
                responseHandler();
                callApiGetBoughtProductByCustomerIdAndProductId(gProductId);
            },
            error: function(xhr) {
                redirectToLogin();
                if(gUser != "-1"){
                    localStorage.clear();
                    localStorage.setItem("user", "-1");
                    gProductListId = [];
                }
                if (gProductListId != null){
                    gNumberProduct.html(gProductListId.length);
                }
            }
        });
    }
    // Hàm gọi sản phẩm theo id
    function callApiGetProductById(){
        "use strict"
        $.ajax({
            url: gProductUrl + gProductId,
            type: "GET",
            dataType: "json",
            async: false,
            success: function(responseObject) {
                gProductObj = responseObject;
                loadProductToForm(responseObject);
                callApiGetFeedbackByProductId(responseObject.id);
                callApiGetRelatedProduct(responseObject);
            },
            error: function(e) {
            }
        });
    }
    // Hàm gọi đánh giá sản phẩm của người dùng
    function callApiGetBoughtProductByCustomerIdAndProductId(paramProductId){
        "use strict"
        $.ajax({
            url: gUrlBoughtProduct + "?productId=" + paramProductId,
            type: "GET",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
                },
            async: false,
            success: function(responseObject) {
                responseBoughtProductHandler(responseObject);
            },
            error: function(e) {
            }
        });
    }
    // Hàm tạo đánh giá sản phẩm cho người dùng
    function callApiCreateFeedback(paramProductId, paramObjFeedback){
        "use strict"
        var vJsonObj = JSON.stringify(paramObjFeedback);
        $.ajax({
            url: gUrlBoughtProduct + "?productId=" + paramProductId,
            type: "PUT",
            contentType: "application/json",
            data: vJsonObj,
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
                },
            async: false,
            success: function(e) {
                $("#comment-form").html('<p id="error" class="error text-success mt-3" style="font-size: 1.6rem;">You just review the product!</p>');
                callApiGetFeedbackByProductId(gProductId);
            },
            error: function(e) {
            }
        });
    }
    // Hàm gọi sản phẩm theo sản phẩm liên quan
    function callApiGetRelatedProduct(paramRes){
        "use strict"
        $.ajax({
            url: gRelatedProductUrl + paramRes.productLine.id,
            type: "GET",
            dataType: "json",
            async: false,
            success: function(responseObject) {
                loadProductToPage(responseObject);
            },
            error: function(e) {
            }
        });
    }
    // Hàm gọi lây đánh giá sản phẩm
    function callApiGetFeedbackByProductId(paramProductId){
        "use strict"
        $.ajax({
            url: gUrlFeedbacks + "?productId=" + paramProductId,
            type: "GET",
            dataType: "json",
            async: false,
            success: function(res) {
                feedbackResHandler(res)
            },
            error: function(e) {
            }
        });
    }
    // Hàm gọi api lấy toàn bộ danh sách yêu thích
    function callApiGetWishList(){
        "use strict"
        $.ajax({
            url: gUrlWishlist,
            type: "GET",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
                },
            success: function(responseObject) {
                wishListHandler(responseObject);
            },
            error: function() {
            }
        });
    }
    // Hàm thêm sản phẩm yêu thích cho người dùng
    function callApiCreateWishItem(paramProductId){
        "use strict"
        $.ajax({
            url: gUrlWishlist + "?productId=" + paramProductId,
            type: "POST",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
                },
            success: function() {
                $("#product-wish-"+ paramProductId +"").html(`
                <i data-id = "`+ paramProductId +`" class="fa-solid fa-heart favourite-icon"></i>
                `)
            },
            error: function() {
            }
        });
    }
    // Hàm xoá sản phẩm yêu thích cho người dùng
    function callApiDeleteWishItem(paramProductId){
        "use strict"
        $.ajax({
            url: gUrlWishlist + "?productId=" + paramProductId,
            type: "DELETE",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
                },
            success: function() {
                $("#product-wish-"+ paramProductId +"").html(`
                <i data-id = "`+ paramProductId +`" class="fa-regular fa-heart unfavourite-icon"></i>
                `)
            },
            error: function() {

            }
        });
    }
    // Hàm gọi api lấy toàn bộ office
    function callApiGetAllOffice(){
        "use strict"
        $.ajax({
            url: gUrlOffice,
            type: "GET",
            dataType: "json",
            success: function(responseObject){
                loadOfficeToPage(responseObject);
            },
            error: function() {
            }
        });
    }
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

    //Hàm get Cookie đã giới thiệu ở bài trước
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
    // Hàm xử lí sau khi đăng nhập
    function responseHandler(){
        "use strict"
        $("#icon-login").addClass("d-none");
        $("#icon-user").removeClass("d-none");
        if(gProductListId != null){
            gNumberProduct.html(gProductListId.length);
        }
    }

    //Hàm logout
    function redirectToLogin() {
        // Trước khi logout cần xóa token đã lưu trong cookie
        setCookie("token", "", 1);
    }
    //Hàm setCookie
    function setCookie(cname, cvalue) {
        document.cookie = cname + "=" + cvalue + ";" + ";path=/";
    }
    // Hàm thêm sản phẩm vào giỏ hàng
    function onBtnAddRelatedToCartClick(paramE){
        "use strict"
        
        var vProductObj = {
            id: "",
            quantity_order: 1,
            priceEach: ""
        }
        
        readProductRelatedData(vProductObj,paramE);
        var bIndex = 0;
        var vFound = false;

        if (gProductListId == null){
            gProductListId = [];
        }
        while(vFound == false &&  bIndex < gProductListId.length){
            if(gProductListId[bIndex].id == vProductObj.id){
                gProductListId[bIndex].quantity_order = gProductListId[bIndex].quantity_order + 1;
                vFound = true;
            }
            else {
                bIndex ++;
            }
        }
         if (vFound == false){
            gProductListId.push(vProductObj);
         }

        gNumberProduct.html(gProductListId.length)

        localStorage.setItem("productList",JSON.stringify(gProductListId));
    }

    //Hàm thu thập dữ liệu khi nhấn thêm sản phẩm
    function readProductRelatedData(paramObj, paramE){
        "use strict"
        paramObj.id = $(paramE).attr("data-id");
        paramObj.priceEach = $(paramE).attr("data-price");
    }

    // Hàm hiển thị thông tin văn phòng
    function loadOfficeToPage(paramRes){
        "use strict"
        var vOfficeContain = $("#store-list");
        vOfficeContain.empty();

        for(let bIndex = 0; bIndex < paramRes.length; bIndex++){
            let bOfficeItem = `
            <div class="col-12 col-sm-6 col-md-3 mt-5">
                <div class="store-item">
                    <p class="store-title"><i class="fa-solid fa-store"></i> Stores - `+ (bIndex + 1) +`</p>
                    <p class="address"><i class="fa-solid fa-location-dot"></i> ` + paramRes[bIndex].address_line + ", " + paramRes[bIndex].city + `</p>
                    <p class="phone"><i class="fa-solid fa-phone-volume"></i> `+ paramRes[bIndex].phone +`</p>
                </div>
            </div>`

            vOfficeContain.append(bOfficeItem);
        }
        gSrTop.reveal(".store-item", {interval: 200});
    }
    
    // Hàm load dữ liệu đánh giá sản phẩm
    function feedbackResHandler(paramRes){
        "use strict"
        var vFeedbackContain = $("#list-feedback");
        vFeedbackContain.empty();
        var vSumRate = 0;
        for(let bIndex = 0; bIndex < paramRes.length; bIndex++){
            vSumRate += paramRes[bIndex].rating;
            var bFeedbackItem = `
            <div class="row review-item">
                <div class="col-12 col-md-1 mb-3">
                    <img src="`+ gUrlDownloadPhoto + "/"+ paramRes[bIndex].customer.photo +`" alt="Avata">
                </div>
                <div class="col-12 col-md-11">
                    <p class="username text-info">`+ paramRes[bIndex].customer.username +`</p>
                    <div class="block-stars-bg mt-2">
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                            </div>
                            <div id="rate-customer-1" class="block-stars-total mt-2" style="--data-rate: `+ paramRes[bIndex].rating +`">
                                <i class="fa-solid fa-star checked"></i>
                                <i class="fa-solid fa-star checked"></i>
                                <i class="fa-solid fa-star checked"></i>
                                <i class="fa-solid fa-star checked"></i>
                                <i class="fa-solid fa-star checked"></i>
                            </div>
                    <div class="feedback-date mt-3 text-danger">`+ paramRes[bIndex].feedbackDate +`</div>
                    <p class="feedback mt-4">`+ paramRes[bIndex].comments +`</p>
                    <hr>
                </div>
            </div>`
            vFeedbackContain.append(bFeedbackItem);
        }

        var vAvarageRate = vSumRate / paramRes.length;
        vAvarageRate = vAvarageRate.toPrecision(2);
        if(paramRes.length > 0){
            $("#total-rate").html(`Total: `+ vAvarageRate +` / 5.0`)
            $("#total-rate-star").attr("style", "--data-rate: "+ vAvarageRate +";")
        }
        
    }

    // Hàm xử lí bình luận người dùng trên sản phẩm
    function responseBoughtProductHandler(paramRes){
        "use strict"
        if(paramRes.rating == null){
            $("#comment-form").removeClass("d-none");
        }
    }
    // Hàm ghi dữ liệu sản vào form
    function  loadProductToForm(paramRes){
        "use strict"

        var vLength = 0;
        if (gProductListId == null){
            vLength = 0;
            gProductListId = [];
        }
        else {
            vLength = gProductListId.length;
        }

        if(paramRes.quantityInStock < 1){
            gQuantity.html(0);
            $("#btn-add-to-cart").prop("disabled", true);
        }

        for(var bIndex = 0; bIndex < vLength; bIndex ++){
            if(paramRes.id == gProductListId[bIndex].id){
                gQuantity.html(gProductListId[bIndex].quantity_order);
            }
        }

        
        $("#photo1").attr("src",gUrlDownloadPhoto + "/" + paramRes.photo1);
        if(paramRes.photo2 != null){
            $("#photo2").attr("src",gUrlDownloadPhoto + "/" + paramRes.photo2);
        }
        else{
            $("#photo2").addClass("d-none");
        }
        if(paramRes.photo3 != null){
            $("#photo3").attr("src",gUrlDownloadPhoto + "/" + paramRes.photo3);
        }
        else{
            $("#photo3").addClass("d-none");
        }
        if(paramRes.photo4 != null){
            $("#photo4").attr("src",gUrlDownloadPhoto + "/" + paramRes.photo4);
        }
        else{
            $("#photo4").addClass("d-none");
        }
        $("#product-name").html( paramRes.productName);
        $("#product-brand").html( "Brand: " + paramRes.productVendor);
        $("#price").html("$"+  paramRes.discountPrice);
        $("#quantity-in-stock").html("Quantity: "+  paramRes.quantityInStock);
        $("#description-product").html( paramRes.productDescription);
        $("#product-link-name").html( paramRes.productName);
        $("#product-line-title").html( paramRes.productLine.productLine);
        $("#product-line-desc").html( paramRes.productLine.description);

        gSrTop.reveal(".img-block", {delay:100});
        gSrTop.reveal(".product-name", {delay:200});
        gSrTop.reveal(".brand", {delay:300});
        gSrTop.reveal(".desc", {delay:400});
        gSrTop.reveal(".price", {delay:500});
        gSrTop.reveal(".quantity-title", {delay:600});
        gSrTop.reveal(".quantity", {delay:700});
        gSrTop.reveal(".btn-add-cart", {delay:800});
        gSrTop.reveal(".description", {delay:900});
        gSrTop.reveal(".review", {delay:900});
    }

    // Hàm xử lí tăng số lượng sản phẩm;
    function plusQuantity(){
        "use strict"
        var vQuantityProduct = $("#quantity-product").html();
        if(vQuantityProduct < gProductObj.quantityInStock){
            $("#quantity-product").html(parseInt(vQuantityProduct) + 1);
        }
    }
    // Hàm xử lí giảm số lượng sản phẩm;
    function minusQuantity(){
        "use strict"
        var vQuantityProduct = $("#quantity-product").html();
        if(vQuantityProduct > 1){
            $("#quantity-product").html(parseInt(vQuantityProduct) - 1);
        }
    }
    // Hàm thu thập dữ liệu sản phẩm
    function readProductDetailsData(paramObj){
        "use strict"
        paramObj.id = gProductObj.id;
        paramObj.quantity_order = $("#quantity-product").html();
        paramObj.priceEach = gProductObj.discountPrice;
    }
    // Hàm xử lí danh sách wishList;
    function wishListHandler(paramRes){
        "use strict"
        paramRes.forEach(wishItem => {
            $("#product-wish-"+ wishItem.product.id +"").html(`
            <i data-id = "`+ wishItem.product.id +`" class="fa-solid fa-heart favourite-icon"></i>
            `)
        });
    }
    // Thêm sản phẩm/ cập nhật sản phẩm trong giỏ hàng
    function addProductToCart(paramObj){
        "use strict"
        var vLength = 0;
        if (gProductListId == null){
            vLength = 0;
            gProductListId = [];
        }
        else {
            vLength = gProductListId.length;
        }

        for(var bIndex = 0; bIndex < vLength; bIndex++){
            if(paramObj.id == gProductListId[bIndex].id){
                gProductListId[bIndex].quantity_order = paramObj.quantity_order;
                localStorage.setItem("productList",JSON.stringify(gProductListId));
                return;
            }
        }
        gProductListId.push(paramObj);
        gNumberProduct.html(gProductListId.length);
        localStorage.setItem("productList",JSON.stringify(gProductListId));
    }
    // Hàm hiển thị dữ liệu sản phẩm lên trang
    function loadProductToPage(paramRes){
        "use strict"
        // Xoá trắng dữ liệu cũ
        var vProductContain = $("#list-product");
        vProductContain.empty();

        var vLength = paramRes.length;
        if(vLength > 8){
            vLength = 8;
        }
        for(var bIndex = 0; bIndex < vLength; bIndex ++){
            var bDisable = "";
            if (paramRes[bIndex].quantityInStock < 1){
                bDisable = "disabled";
            }
            var vPrice = `<p class="price"><span class="discount">$` + paramRes[bIndex].buyPrice + `</span> $` + paramRes[bIndex].discountPrice + `</p>`

            if(paramRes[bIndex].discountPrice >= paramRes[bIndex].buyPrice ){
                vPrice = `<p class="price text-dark">$` + paramRes[bIndex].discountPrice + `</p>`
            }
            var bProduct = `<div class="col-12 col-lg-3 col-md-4 col-sm-6">
            <div class="item">
                <a href="./product-details.html?id=` + paramRes[bIndex].id + `">
                    <img src="` + gUrlDownloadPhoto + "/" + paramRes[bIndex].photo1 + `" alt="` + paramRes[bIndex].productName + `"/>
                </a>
                <p class="product-name">` + paramRes[bIndex].productName + `</p>
                `+ vPrice +`
                <p id = "stock-id-` + paramRes[bIndex].id + `" class="stock">Quantity in stock: ` + paramRes[bIndex].quantityInStock + ` </p>
                <div class="cart-block">
                    <button  `+ bDisable +` id="btn-add" data-price = "` + paramRes[bIndex].discountPrice + `" data-id = "` + paramRes[bIndex].id + `" class="btn-add-cart"><i class="fa-solid fa-cart-plus"></i></button>
                    <div class="wish-list-block" id = "product-wish-`+ paramRes[bIndex].id +`">
                        <i data-id = "`+ paramRes[bIndex].id +`" class="fa-regular fa-heart unfavourite-icon"></i>
                    </div>
                </div>
            </div>
            </div>`

            vProductContain.append(bProduct);
        }
        callApiGetWishList();
        gSrTop.reveal(".item", {interval:150});
    }
});