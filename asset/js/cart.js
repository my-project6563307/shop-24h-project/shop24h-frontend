$(document).ready(function(){
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
        const vtoken = getCookie("token");
        var urlInfo = "http://localhost:8080/shop24h/api/noauth/customers";

        var gProductUrl = "http://localhost:8080/shop24h/api/noauth/products/detail/";

        var gOrderUrl = "http://localhost:8080/shop24h/api/noauth/orders";

        var gOrderDetailUrl = "http://localhost:8080/shop24h/api/noauth/order-details";

        var gCustomerCreateUrl = "http://localhost:8080/shop24h/api/noauth/customers";

        var gUrlOffice = "http://localhost:8080/shop24h/api/noauth/offices";

        var gUrlDownloadPhoto = "http://localhost:8080/shop24h/api/noauth/downloadFile";
        
        var gNumberProduct = $("#number-product");
        
        var gUser = localStorage.getItem("user");

        var gUserLogin = false;
    
        var gProductListId = JSON.parse(localStorage.getItem("productList"));

        var gUserInfor = "";

        // Scroll interval
        const gSrTop = ScrollReveal({
            origin: "left",
            distance : "120px",
            duration : 1000,
            reset: false
        })
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
        onPageLoading();
    
        $(document).on("click", "#btn-signout", function(){

            redirectToLogin();
            window.location.href = "./login.html"
        })
        $(document).on("click", "#icon-user", function(){
            $("#user-info").removeClass("d-none");
            $("#icon-user").addClass("open");
        })
        $(document).on("click", "#icon-user.open", function(){
            $("#user-info").addClass("d-none");
            $("#icon-user").removeClass("open");
        })
        // Tạo sự kiện nhấn nút cộng số lượng sản phẩm
        $(document).on("click", ".quantity-plus", function(){
            onIconPlusClick(this);
            calculateCart();
        })
        // Tạo sự kiện nhấn nút giảm số lượng sản phẩm
        $(document).on("click", ".quantity-minus", function(){
            onIconMinusClick(this);
            calculateCart();
        })
        // Tạo sự kiện nhấn nút xoá sản phẩm
        $(document).on("click", ".icon-remove", function(){
            onIconRemoveClick(this);
            calculateCart();
        })
        // Tạo sự kiện nhấn nút check out
        $(document).on("click", "#btn-checkout", function(){
            onBtnCheckOutClick();
        })
        // Tạo sự kiện nhấn nút đóng modal
        $(document).on("click", "#icon-close-modal", function(){
            $("#modal-noti").addClass("d-none");
        })
        // Tạo sự kiện nhấn nút đóng form
        $(document).on("click", "#close-form", function(){
            $("#user-form").addClass("d-none");
        })
        // Tạo sự kiện nhấn nút xác nhận đặt hàng
        $(document).on("click", "#btn-place-order", function(){
            onBtnPlaceOrderClick();
        })
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
        function onPageLoading(){
            "use strict"
            callApiCheckUser();
            if (gProductListId != null && gProductListId.length > 0){
                callApiGetProductById();
                calculateCart();
            }
            else {
                $("#list-products").empty();
                $("#list-products").html("<h3 class ='text-danger'>No products in your cart</h3>"
                + '<a class="nav-link btn-shop-now" href="./list-product.html">SHOP NOW</a>');
                $("#btn-checkout").addClass("d-none");
                $(".comments").addClass("d-none");
                $(".total").addClass("d-none");

            }
            callApiGetAllOffice();
            // // Scroll reveal
            gSrTop.reveal(".path");
        }
    
        // Hàm xử lí nhấn nút check out 
        function onBtnCheckOutClick(){
            "use strict"
            if(gUserLogin == true){
                loadUserInforToForm(gUserInfor);

                $("#user-form").removeClass("d-none");
                
            }
            else {
                $("#user-form").removeClass("d-none");
            }
        }

        // Hàm xử lí nhấn nút đặt hàng
        function onBtnPlaceOrderClick(){
            "use strict"
            var vOrderObj = {
                comments: "",
                deliveryAddress:""
            }
            var vNewUser = {
                firstName:"",
                lastName: "",
                phoneNumber: "",
                address:""
            }
            //thu thập dữ liệu
            readOrderData(vOrderObj);

            readUserData(vNewUser);

            //xử lí dữ liệu
            var vValid = validateOrder(vOrderObj);
            if (vValid && gUserLogin == true){
                callApiCreateOrder(gUser, vOrderObj);
            }
            else if (vValid && gUserLogin == false){
                callApiCheckUserToCreateOrder(vNewUser,vOrderObj);
            }
        }
        // Hàm xử lí nhấn nút thêm số lượng
        function onIconPlusClick(paramE){
            "use strict"
            var vObj = {
                id: "",
                stock: ""
            }
            getIdProductInCart(paramE,vObj);

            plusQuantity(vObj);
        }
        // Hàm xử lí nhấn nút giảm số lượng
        function onIconMinusClick(paramE){
            "use strict"
            var vObj = {
                id: "",
                stock: ""
            }
            getIdProductInCart(paramE,vObj);

            minusQuantity(vObj);
        }
        function onIconRemoveClick(paramE){
            "use strict"
            var vObj = {
                id: "",
                stock: ""
            }
            getIdProductInCart(paramE,vObj);

            removeProduct(vObj);
        }
        // Hàm kiểm tra người dùng đăng nhập
        function callApiCheckUser(){
            $.ajax({
                url: urlInfo,
                type: "GET",
                dataType: "json",
                async: false,
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                    },
                success: function(responseObject) {
                    if(gUser != responseObject.id){
                        localStorage.clear();
                        localStorage.setItem("user", responseObject.id);
                        gProductListId = [];
                    }
                    gUserLogin = true;
                    gUserInfor =  responseObject;
                    responseHandler();
                },
                error: function(xhr) {
                    redirectToLogin();
                    if(gUser != "-1"){
                        localStorage.clear();
                        localStorage.setItem("user", "-1");
                        gProductListId = [];
                    }
                    if (gProductListId != null){
                        gNumberProduct.html(gProductListId.length);
                    }
                }
            });
        }
        // Hàm gọi sản phẩm theo id
        function callApiGetProductById(){
            "use strict"
            $("#list-products").empty();

            for (var bIndex = 0; bIndex < gProductListId.length; bIndex ++){
                $.ajax({
                    url: gProductUrl + gProductListId[bIndex].id,
                    type: "GET",
                    dataType: "json",
                    success: function(responseObject) {
                        loadProductToCart(responseObject);
                    },
                    error: function(e) {
                    }
                });
            }
        }
        // Hàm tạo order theo customerid
        function callApiCheckUserToCreateOrder(paramUserObj,paramOrderObj){
            "use strict"
            $.ajax({
                url: gCustomerCreateUrl,
                type: "POST",
                contentType: "application/json",
                data:JSON.stringify(paramUserObj),
                success: function(responseObject) {
                    callApiCreateOrder(responseObject.id, paramOrderObj);
                },
                error: function(xhr) {
                }
            });
        }
        // Hàm tạo order theo customerid
        function callApiCreateOrder(paramUserId,paramOrder){
            "use strict"
            $.ajax({
                url: gOrderUrl + "?customerId=" + paramUserId,
                type: "POST",
                contentType: "application/json",
                data:JSON.stringify(paramOrder),
                success: function(responseObject) {
                    callApiCreateOrderDetails(responseObject);
                },
                error: function() {
                }
            });
        }

        // Hàm tạo orderdetails theo order
        function callApiCreateOrderDetails(paramRes){
            "use strict"
            gProductListId.forEach( product => {
                $.ajax({
                    url: gOrderDetailUrl + "?orderId=" + paramRes.id
                    + "&productId=" + product.id,
                    type: "POST",
                    contentType: "application/json",
                    data:JSON.stringify(product),
                    success: function(responseObject) {
                        responseHandlerCart();
                    },
                    error: function(xhr) {
                    }
                });
            });
        }
        // Hàm gọi api lấy toàn bộ office
        function callApiGetAllOffice(){
            "use strict"
            $.ajax({
                url: gUrlOffice,
                type: "GET",
                dataType: "json",
                success: function(responseObject){
                    loadOfficeToPage(responseObject);
                },
                error: function() {
                }
            });
        }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

        //Hàm get Cookie đã giới thiệu ở bài trước
        function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }
        // Hàm hiển thị thông tin văn phòng
        function loadOfficeToPage(paramRes){
            "use strict"
            var vOfficeContain = $("#store-list");
            vOfficeContain.empty();

            for(let bIndex = 0; bIndex < paramRes.length; bIndex++){
                let bOfficeItem = `
                <div class="col-12 col-sm-6 col-md-3 mt-5">
                    <div class="store-item">
                        <p class="store-title"><i class="fa-solid fa-store"></i> Stores - `+ (bIndex + 1) +`</p>
                        <p class="address"><i class="fa-solid fa-location-dot"></i> ` + paramRes[bIndex].address_line + ", " + paramRes[bIndex].city + `</p>
                        <p class="phone"><i class="fa-solid fa-phone-volume"></i> `+ paramRes[bIndex].phone +`</p>
                    </div>
                </div>`

                vOfficeContain.append(bOfficeItem);
            }
            gSrTop.reveal(".store-item", {interval: 200});
        }
        // Hàm xử lí sau khi đăng nhập
        function responseHandler(){
            "use strict"
            $("#icon-login").addClass("d-none");
            $("#icon-user").removeClass("d-none");
            if(gProductListId != null){
                gNumberProduct.html(gProductListId.length);
            }
        }
    
        // Hàm xử lí ghi dữ liệu người dùng lên form tạo đơn
        function loadUserInforToForm(paramObj){
            "use strict"
            $("#inp-firstname").val(paramObj.firstName);
            $("#inp-lastname").val(paramObj.lastName);
            $("#inp-phone").val(paramObj.phoneNumber);
            $("#inp-address").val(paramObj.address);
        }

        // Hàm xử lí thu thập dữ liệu từ form tạo đơn
        function readOrderData(paramObj){
            "use strict"

            paramObj.comments = $("#inp-comments").val().trim();
            paramObj.deliveryAddress = $("#inp-firstname").val().trim() 
            + " " + $("#inp-lastname").val().trim()
            + ", " + $("#inp-phone").val().trim()
            + ", " + $("#inp-address").val().trim();
        }

        // Hàm thu thập dữ liệu từ form cho người dùng mới
        function readUserData(paramObj){
            "use strict"
            paramObj.firstName = $("#inp-firstname").val().trim();
            paramObj.lastName = $("#inp-lastname").val().trim();
            paramObj.phoneNumber = $("#inp-phone").val().trim();
            paramObj.address = $("#inp-address").val().trim();
        }
        // Hàm xử lí thu thập dữ liệu từ form tạo đơn
        function validateOrder(){
            "use strict";
            if($("#inp-firstname").val() == ""){
                $("#error").html("Please fill out first name!");
                $("#inp-firstname").focus();
                return false;
            }
            if($("#inp-lastname").val() == ""){
                $("#error").html("Please fill out last name!");
                $("#inp-lastname").focus();
                return false
            }
            if($("#inp-phone").val() == ""){
                $("#error").html("Please fill out phone number!");
                $("#inp-phone").focus();
                return false
            }
            if($("#inp-address").val() == ""){
                $("#error").html("Please fill out address!");
                $("#inp-address").focus();
                return false
            }
        $("#error").html("");
        return true;
        }
        //Hàm logout
        function redirectToLogin() {
            // Trước khi logout cần xóa token đã lưu trong cookie
            setCookie("token", "", 1);
        }
        //Hàm setCookie
        function setCookie(cname, cvalue) {
            document.cookie = cname + "=" + cvalue + ";" + ";path=/";
        }
        //Hàm xử lí sau khi tạo đơn

        function responseHandlerCart(){
            "use strict"
            // empty cart
            $("#list-products").empty();
            $("#list-products").html("<h3 class ='text-danger'>No products in your cart</h3>"
            + '<a class="nav-link btn-shop-now" href="./list-product.html">SHOP NOW</a>');
            $("#btn-checkout").addClass("d-none");
            $(".comments").addClass("d-none");

            // empty local storerage
            gProductListId = [];
            localStorage.setItem("productList",JSON.stringify(gProductListId));
            gNumberProduct.html(gProductListId.length);

            // hide form
            $("#user-form").addClass("d-none");

            // display modal
            $("#modal-noti").removeClass("d-none");
        }

        // Hàm load sản phẩm trong giỏ hàng
        function loadProductToCart(paramObj){
            "use strict"
            var vQuantity = "";

            for(var i = 0; i < gProductListId.length; i++){
                if (gProductListId[i].id == paramObj.id){
                    vQuantity = gProductListId[i].quantity_order;
                }
            }
            var vProductContains = $("#list-products");
            var vContent = `<div class=" cart-item row" id = "product-` +paramObj.id+ `">
            <div class="col-lg-3 col-md-3 col-sm-4 col-4 mt-2">
                <img src="` + gUrlDownloadPhoto + "/" + paramObj.photo1 + `" alt="Photo" class="product-img">
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-4 mt-2">
                <p class="product-name">` + paramObj.productName + `</p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-4 mt-2">
                <p class="price text-danger">` + paramObj.discountPrice + ` $</p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-4 mt-2">
                <div id ="quantity-block" class="quantity" data-stock = "` + paramObj.quantityInStock + `" data-id = "` + paramObj.id + `" data-quantity = "` + vQuantity + `">
                    <i class="fa-solid fa-minus quantity-minus"></i>
                    <p id = "product-quantity-` + paramObj.id + `" >` + vQuantity + `</p>
                    <i class="fa-solid fa-plus quantity-plus"></i>
                    <i class="fa-solid fa-ban icon-remove"></i>
                </div>
            </div>
            </div>`

            vProductContains.append(vContent);
            gSrTop.reveal(".cart-item",{interval:200});
            gSrTop.reveal(".footer");
        }

        // Hàm lấy id sảm phẩm
        function getIdProductInCart(paramE, paramObj){
            var vParent = $(paramE).parent(".quantity");
            paramObj.id = vParent.attr("data-id");
            paramObj.stock = vParent.attr("data-stock");
        }

        // Hàm xử lí tăng số lượng sản phẩm;
        function plusQuantity(paramObj){
            "use strict"
            for(var bIndex = 0; bIndex < gProductListId.length; bIndex++){
                if(paramObj.id == gProductListId[bIndex].id && gProductListId[bIndex].quantity_order < paramObj.stock){
                    gProductListId[bIndex].quantity_order = gProductListId[bIndex].quantity_order + 1;
                    $("#product-quantity-"+ paramObj.id +"").empty();
                    $("#product-quantity-"+ paramObj.id +"").html(gProductListId[bIndex].quantity_order);
                    localStorage.setItem("productList",JSON.stringify(gProductListId));
                }
            }
        }
        // Hàm xử lí giảm số lượng sản phẩm;
        function minusQuantity(paramObj){
            "use strict"
            for(var bIndex = 0; bIndex < gProductListId.length; bIndex++){
                if(paramObj.id == gProductListId[bIndex].id && gProductListId[bIndex].quantity_order > 1){
                    gProductListId[bIndex].quantity_order = gProductListId[bIndex].quantity_order - 1;
                    $("#product-quantity-"+ paramObj.id +"").empty();
                    $("#product-quantity-"+ paramObj.id +"").html(gProductListId[bIndex].quantity_order);
                    localStorage.setItem("productList",JSON.stringify(gProductListId));
                }
            }
        }

        // Hàm xử lí xoá sản phẩm trong giỏ hàng
        function removeProduct(paramObj){
            "use strict"
            for(var bIndex = 0; bIndex < gProductListId.length; bIndex++){
                if(paramObj.id == gProductListId[bIndex].id){
                    gProductListId.splice(bIndex,1);
                    localStorage.setItem("productList",JSON.stringify(gProductListId));
                    $("#product-"+ paramObj.id +"").addClass("d-none");
                    gNumberProduct.html(gProductListId.length);
                    if (gProductListId.length == 0){
                        $(".comments").addClass("d-none");
                        $("#btn-checkout").addClass("d-none");
                        $("#list-products").empty();
                        $("#list-products").html("<h3 class ='text-danger'>No products in your cart</h3>"
                        + '<a class="nav-link btn-shop-now" href="./list-product.html">SHOP NOW</a>');
                    }
                    return;
                }
            }
        }

        // Hàm xử lí tính tổng giá trị giỏ hàng
        function calculateCart(){
            "use trict"
            var vSumProduct = gProductListId.length;
            var vSumPrice = 0;
            for(let bIndex = 0; bIndex < gProductListId.length; bIndex ++){
                vSumPrice = vSumPrice + (gProductListId[bIndex].priceEach * gProductListId[bIndex].quantity_order);
            }

            $("#inp-product").val(vSumProduct);
            $("#inp-price").val(vSumPrice + " $");
        }
    });