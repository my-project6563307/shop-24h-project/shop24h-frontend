$(document).ready(function(){
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
        const vtoken = getCookie("token");

        var urlInfo = "http://localhost:8080/shop24h/api/noauth/customers";
    
        var gProductUrl = "http://localhost:8080/shop24h/api/noauth/products/detail/";
    
        var gUrlWishlist = "http://localhost:8080/shop24h/wishlists";

        var gUrlOffice = "http://localhost:8080/shop24h/api/noauth/offices";

        var gUrlDownloadPhoto = "http://localhost:8080/shop24h/api/noauth/downloadFile";
    
        var gNumberProduct = $("#number-product");
    
        var gUser = localStorage.getItem("user");
    
        var gProductListId = JSON.parse(localStorage.getItem("productList"));

        var gProductObj = "";
    
        // Scroll interval
        const gSrTop = ScrollReveal({
            origin: "top",
            distance : "60px",
            duration : 1000,
            reset: false
        })
    
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
        onPageLoading();
    
        $(document).on("click", "#btn-signout", function(){
            redirectToLogin();
            window.location.href = "./login.html"
        })
        $(document).on("click", "#icon-user", function(){
            $("#user-info").removeClass("d-none");
            $("#icon-user").addClass("open");
        })
        $(document).on("click", "#icon-user.open", function(){
            $("#user-info").addClass("d-none");
            $("#icon-user").removeClass("open");
        })
        // Tạo sự kiện nhấn nút thêm sản phẩm vào giỏ hàng
        $(document).on("click", "#btn-add", function(){
            onBtnAddToCartClick(this);
        })
        // Tạo sự kiện nhấn icon thêm sản phẩm vào danh sách yêu thích
        $(document).on("click", ".unfavourite-icon", function(){
            onBtnAddWishListClick(this);
        })
        // Tạo sự kiện nhấn icon bỏ sản phẩm yêu thích
        $(document).on("click", ".favourite-icon", function(){
            onBtnRemoveWishListClick(this);
        })
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
        function onPageLoading(){
            "use strict"
            callApiCheckUser();
    
            callApiGetWishList();

            callApiGetAllOffice();

            gSrTop.reveal(".path");
            gSrTop.reveal(".wishlist-header", {delay: 200});
        }
        // Hàm xử lí nhấn nút thêm sản phẩm vào giỏ hàng
        // Hàm thêm sản phẩm vào giỏ hàng
    function onBtnAddToCartClick(paramE){
        "use strict"
        
        var vProductObj = {
            id: "",
            quantity_order: 1,
            priceEach: 0
        }
        var vQuantityStock = $(paramE).attr("data-stock");

        readProductData(vProductObj,paramE,vQuantityStock);

        var bIndex = 0;
        var vFound = false;

        if (gProductListId == null){
            gProductListId = [];
        }

        while(vFound == false &&  bIndex < gProductListId.length){
            if(gProductListId[bIndex].id == vProductObj.id){
                if (gProductListId[bIndex].quantity_order < vQuantityStock){
                    gProductListId[bIndex].quantity_order = gProductListId[bIndex].quantity_order + 1;
                }
                vFound = true;
            }
            else {
                bIndex ++;
            }
        }
         if (vFound == false){
            gProductListId.push(vProductObj);
         }

        gNumberProduct.html(gProductListId.length);

        localStorage.setItem("productList",JSON.stringify(gProductListId));
    }
        // Hàm xử lí thêm sản phẩm yêu thích
        function onBtnAddWishListClick(paramIcon){
            "use strict"
            var vProductId = $(paramIcon).data("id");
            if(gUser != -1){
                callApiCreateWishItem(vProductId);
            }
        }
        // Hàm xử lí bỏ sản phẩm yêu thích
        function onBtnRemoveWishListClick(paramIcon){
            "use strict"
            var vProductId = $(paramIcon).data("id");
            if(gUser != -1){
                callApiDeleteWishItem(vProductId);
            }
        }
    
        // Hàm kiểm tra người dùng đăng nhập
        function callApiCheckUser(){
            $.ajax({
                url: urlInfo,
                type: "GET",
                dataType: "json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                    },
                success: function(responseObject) {
                    if(gUser != responseObject.id){
                        localStorage.clear();
                        localStorage.setItem("user", responseObject.id);
                        gProductListId = [];
                    }
                    responseHandler();
                },
                error: function(xhr) {
                    redirectToLogin();
                    if(gUser != "-1"){
                        localStorage.clear();
                        localStorage.setItem("user", "-1");
                        gProductListId = [];
                    }
                    if (gProductListId != null){
                        gNumberProduct.html(gProductListId.length);
                    }
                }
            });
        }
        // Hàm gọi sản phẩm theo id
        function callApiGetProductById(paramId){
            "use strict"
            $.ajax({
                url: gProductUrl + paramId,
                type: "GET",
                dataType: "json",
                async: false,
                success: function(responseObject) {
                    gProductObj = responseObject;
                    loadProductToPage(responseObject);
                },
                error: function(e) {
                }
            });
        }
        // Hàm gọi api lấy toàn bộ danh sách yêu thích
        function callApiGetWishList(){
            "use strict"
            $.ajax({
                url: gUrlWishlist,
                type: "GET",
                dataType: "json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                    },
                success: function(responseObject) {
                    wishListHandler(responseObject);
                },
                error: function() {
                    $("#list-product").html(`<h2 class="text-danger text-center">NO PRODUCTS IN YOURS WISHLISH!</h2>`);
                }
            });
        }
        // Hàm thêm sản phẩm yêu thích cho người dùng
        function callApiCreateWishItem(paramProductId){
            "use strict"
            $.ajax({
                url: gUrlWishlist + "?productId=" + paramProductId,
                type: "POST",
                dataType: "json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                    },
                success: function() {
                    $("#product-wish-"+ paramProductId +"").html(`
                    <i data-id = "`+ paramProductId +`" class="fa-solid fa-heart favourite-icon"></i>
                    `)
                },
                error: function() {
                }
            });
        }
        // Hàm xoá sản phẩm yêu thích cho người dùng
        function callApiDeleteWishItem(paramProductId){
            "use strict"
            $.ajax({
                url: gUrlWishlist + "?productId=" + paramProductId,
                type: "DELETE",
                dataType: "json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                    },
                success: function() {
                    $("#product-wish-"+ paramProductId +"").html(`
                    <i data-id = "`+ paramProductId +`" class="fa-regular fa-heart unfavourite-icon"></i>
                    `)
                },
                error: function() {
                }
            });
        }

        // Hàm gọi api lấy toàn bộ office
        function callApiGetAllOffice(){
            "use strict"
            $.ajax({
                url: gUrlOffice,
                type: "GET",
                dataType: "json",
                success: function(responseObject){
                    loadOfficeToPage(responseObject);
                },
                error: function() {
                }
            });
        }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    
        //Hàm get Cookie đã giới thiệu ở bài trước
        function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }
        // Hàm hiển thị thông tin văn phòng
        function loadOfficeToPage(paramRes){
            "use strict"
            var vOfficeContain = $("#store-list");
            vOfficeContain.empty();

            for(let bIndex = 0; bIndex < paramRes.length; bIndex++){
                let bOfficeItem = `
                <div class="col-12 col-sm-6 col-md-3 mt-5">
                    <div class="store-item">
                        <p class="store-title"><i class="fa-solid fa-store"></i> Stores - `+ (bIndex + 1) +`</p>
                        <p class="address"><i class="fa-solid fa-location-dot"></i> ` + paramRes[bIndex].address_line + ", " + paramRes[bIndex].city + `</p>
                        <p class="phone"><i class="fa-solid fa-phone-volume"></i> `+ paramRes[bIndex].phone +`</p>
                    </div>
                </div>`

                vOfficeContain.append(bOfficeItem);
            }
        }
        // Hàm xử lí sau khi đăng nhập
        function responseHandler(){
            "use strict"
            $("#icon-login").addClass("d-none");
            $("#icon-user").removeClass("d-none");
            if(gProductListId != null){
                gNumberProduct.html(gProductListId.length);
            }
        }
        //Hàm logout
        function redirectToLogin() {
            // Trước khi logout cần xóa token đã lưu trong cookie
            setCookie("token", "", 1);
        }
        //Hàm setCookie
        function setCookie(cname, cvalue) {
            document.cookie = cname + "=" + cvalue + ";" + ";path=/";
        }
        // Hàm xử lí danh sách wishList;
        function wishListHandler(paramRes){
            "use strict"
            // Xoá trắng dữ liệu cũ
            var vProductContain = $("#list-product");
            vProductContain.empty()
            paramRes.forEach(wishItem => {
                callApiGetProductById(wishItem.product.id);
            });
            gSrTop.reveal(".item", {interval: 100});
        }
        //Hàm thu thập dữ liệu khi nhấn thêm sản phẩm
        function readProductData(paramObj, paramE){
            "use strict"
            paramObj.id = $(paramE).attr("data-id");
            paramObj.priceEach = $(paramE).attr("data-price");
        }
        // Hàm hiển thị dữ liệu sản phẩm lên trang
        function loadProductToPage(paramRes){
            "use strict";
            var vProductContain = $("#list-product");

            var bDisable = "";
            if (paramRes.quantityInStock < 1){
                bDisable = "disabled";
            }
            var vPrice = `<p class="price"><span class="discount">$` + paramRes.buyPrice + `</span> $` + paramRes.discountPrice + `</p>`

            if(paramRes.discountPrice >= paramRes.buyPrice ){
                vPrice = `<p class="price text-dark">$` + paramRes.discountPrice + `</p>`
            }
            var bProduct = `<div class="col-6 col-lg-3 col-md-4 col-sm-4 mt-3">
            <div class="item">
                <a href="./product-details.html?id=` + paramRes.id + `">
                    <img src="` + gUrlDownloadPhoto + "/" + paramRes.photo1 + `" alt="` + paramRes.productName + `"/>
                </a>
                <p class="product-name line-clamp-1">` + paramRes.productName + `</p>
                `+ vPrice +`
                <p id = "stock-id-` + paramRes.id + `" class="stock">Quantity in stock: ` + paramRes.quantityInStock + ` </p>
                <div class="cart-block">
                    <button  `+ bDisable +` id="btn-add" data-price = "` + paramRes.discountPrice + `" data-id = "` + paramRes.id + `" class="btn-add-cart"><i class="fa-solid fa-cart-plus"></i></button>
                    <div class="wish-list-block" id = "product-wish-`+ paramRes.id +`">
                        <i data-id = "`+ paramRes.id +`" class="fa-solid fa-heart favourite-icon"></i>
                    </div>
                </div>
            </div>
            </div>`
            vProductContain.append(bProduct);
        }
    });