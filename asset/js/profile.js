$(document).ready(function(){
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
        const vtoken = getCookie("token");
        var urlInfo = "http://localhost:8080/shop24h/api/noauth/customers";

        var gUrlUpdateUser = "http://localhost:8080/shop24h/customers/profile";

        var gUrlUpdateUserPassword = "http://localhost:8080/shop24h/customers/password";

        var gUrlOffice = "http://localhost:8080/shop24h/api/noauth/offices";

        var gUrlDownloadPhoto = "http://localhost:8080/shop24h/api/noauth/downloadFile";

        var gUrlUploadPhoto = "http://localhost:8080/shop24h/api/noauth/uploadFile";
    
        var gNumberProduct = $("#number-product");
    
        var gUser = localStorage.getItem("user");
    
        var gProductListId = JSON.parse(localStorage.getItem("productList"));

        // Scroll interval
        const gSrTop = ScrollReveal({
            origin: "left",
            distance : "60px",
            duration : 1000,
            reset: false
        })
    
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
        // Sự kiện load trang
        onPageLoading();
    
        $(document).on("click", "#btn-signout", function(){
            redirectToLogin();
            window.location.href = "./login.html"
        })
        $(document).on("click", "#icon-user", function(){
            $("#user-info").removeClass("d-none");
            $("#icon-user").addClass("open");
        })
        $(document).on("click", "#icon-user.open", function(){
            $("#user-info").addClass("d-none");
            $("#icon-user").removeClass("open");
        })

        // Tạo sự kiện  nhấn nút lưu thông tin
        $(document).on("click", "#btn-save", function(){
            onBtnSaveClick();
        })
        // Tạo sự kiện thay đổi mật khẩu
        $(document).on("click", "#btn-change-password", function(){
            onBtnChangePasswordClick();
        })
        // Tạo sự kiện tải ảnh cá nhân
        $(document).on("change", "#fileInput", function(){
            uploadImage();
        })

        // Modal 
        $(document).on("click", ".close", () => {
            $("#notification-modal").addClass("d-none");
        })
        $(document).on("click", "#notification-modal", () => {
            $("#notification-modal").addClass("d-none");
        })
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
        function onPageLoading(){
            "use strict"
            callApiCheckUser();

            callApiGetAllOffice();

            gSrTop.reveal(".path");
            gSrTop.reveal(".user-header",{delay: 200});
            gSrTop.reveal(".user-body",{delay: 400});
        }
        // Hàm xử lí thay đổi mật khẩu
        function onBtnChangePasswordClick(){
            "use strict"
            var vPasswordObj = {
                oldPassword : "",
                newPassword : "",
                repeatPassword : "",
            }

            //thu thập dữ liệu
            readPasswordData(vPasswordObj);
            // Kiểm tra dữ liệu
            let vValid = validatePassword(vPasswordObj);
            if(vValid){
                callApiUpdatePassword(vPasswordObj);
            }
        }
        // Hàm xử lí nhấn nút đổi hình ảnh
        function uploadImage() {
            var fileInput = document.getElementById("fileInput");
            const previewImage = document.getElementById('user-img');

            const selectedFile = fileInput.files[0];
            const reader = new FileReader();

            if (selectedFile) {
                reader.readAsDataURL(selectedFile);
            }
            reader.addEventListener('load', () => {
                previewImage.src = reader.result;
            });
        }
        // Hàm upload hình ảnh
        function callApiUploadPhoto(){
            "use strict"
            var fileInput = document.getElementById("fileInput");
            var selectedFile = fileInput.files[0];
            var form = new FormData();
            form.append("file", selectedFile, selectedFile.name);

            $.ajax({
                "url": gUrlUploadPhoto,
                "method": "POST",
                "timeout": 0,
                "processData": false,
                "mimeType": "multipart/form-data",
                "contentType": false,
                "data": form,
                async: false,
                success: function() {
                },
                error: function() {
                }
            });
        }
        // Hàm xử lí nhấn nút lưu dữ liệu người dùng
        function onBtnSaveClick(){
            "use strict"
            var vUpdateUserObj = {
                firstName: "",
                lastName: "",
                phoneNumber: "",
                email: "",
                address: "",
                city: "",
                country: "",
                photo: ""
            }
            // Thu thập dữ liệu
            readUserInfor(vUpdateUserObj);
            // Call Api update user
            callApiUpdateUser(vUpdateUserObj);
            callApiUploadPhoto(vUpdateUserObj.photo);
        }
        // Hàm gọp api cập nhật mật khẩu
        function callApiUpdatePassword(paramPassword){
            "use strict"
            $.ajax({
                url: gUrlUpdateUserPassword
                + "?oldPassword=" + paramPassword.oldPassword
                +"&newPassword=" + paramPassword.newPassword,
                type: "PUT",
                dataType: "application/json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                    },
                success: function() {
                    $("#desc-noti").html("Change password successfully");
                    $("#notification-modal").removeClass("d-none");
                },
                error: function(xhr) {
                    $("#desc-noti").html(xhr.responseText);
                    $("#notification-modal").removeClass("d-none");
                }
            });
        }
        // Hàm gọp api cập nhật người dùng
        function callApiUpdateUser(paramUser){
            "use strict"
            let vJsonObj = JSON.stringify(paramUser);
            $.ajax({
                url: gUrlUpdateUser,
                type: "PUT",
                contentType: "application/json",
                data: vJsonObj,
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                    },
                success: function(res) {

                    $("#desc-noti").html("Save your information successfully");
                    $("#notification-modal").removeClass("d-none");
                },
                error: function(er) {
                    $("#desc-noti").html(er.responseJSON.message);
                    $("#notification-modal").removeClass("d-none");
                }
            });
        }
        // Hàm kiểm tra người dùng đăng nhập
        function callApiCheckUser(){
            $.ajax({
                url: urlInfo,
                type: "GET",
                dataType: "json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                    },
                success: function(responseObject) {
                    if(gUser != responseObject.id){
                        localStorage.clear();
                        localStorage.setItem("user", responseObject.id);
                        gProductListId = [];
                    }
                    responseHandler(responseObject);
                },
                error: function(xhr) {
                    redirectToLogin();
                    if(gUser != "-1"){
                        localStorage.clear();
                        localStorage.setItem("user", "-1");
                        gProductListId = [];
                    }
                    if (gProductListId != null){
                        gNumberProduct.html(gProductListId.length);
                    }
                }
            });
        }

        // Hàm gọi api lấy toàn bộ office
        function callApiGetAllOffice(){
            "use strict"
            $.ajax({
                url: gUrlOffice,
                type: "GET",
                dataType: "json",
                success: function(responseObject){
                    loadOfficeToPage(responseObject);
                },
                error: function() {
                }
            });
        }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

        // Hàm kiểm tra dữ liệu password
        function validatePassword(paramObj){
            "use strict"
            if(paramObj.oldPassword == ""){
                $("#password-error").html("Must fill out old password");
                return false;
            }
            if(paramObj.newPassword == ""){
                $("#password-error").html("Must fill out new password");
                return false;
            }
            if(paramObj.repeatPassword == ""){
                $("#password-error").html("Must fill out repeat password");
                return false;
            }
            if(paramObj.newPassword.length < 7){
                $("#password-error").html("New password must more 7 letters");
                return false;
            }
            if(paramObj.newPassword != paramObj.repeatPassword){
                $("#password-error").html("Repeat password not matched");
                return false;
            }
            $("#password-error").html("------------------");
            return true
        }
        //  Hàm thu thập dữ liệu password
        function readPasswordData(paramObj){
            "use strict"
            paramObj.oldPassword = $("#inp-old-password").val().trim();
            paramObj.newPassword = $("#inp-new-password").val().trim();
            paramObj.repeatPassword = $("#inp-repeat-password").val().trim();
        }
        // Hm thu thập dữ liệu người dùng
        function readUserInfor(paramUser){
            "use strict"
            paramUser.firstName = $("#inp-firstname").val().trim();
            paramUser.lastName = $("#inp-lastname").val().trim();
            paramUser.phoneNumber = $("#inp-phone").val().trim();
            paramUser.email = $("#inp-email").val().trim();
            paramUser.address = $("#inp-address").val().trim();
            paramUser.city = $("#inp-city").val().trim();
            paramUser.country = $("#inp-country").val().trim();

            var fileInput = document.getElementById("fileInput");
            var selectedFile = fileInput.files[0];
            paramUser.photo = selectedFile.name;
        }
        //Hàm get Cookie đã giới thiệu ở bài trước
        function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }
        // Hàm hiển thị thông tin văn phòng
        function loadOfficeToPage(paramRes){
            "use strict"
            var vOfficeContain = $("#store-list");
            vOfficeContain.empty();

            for(let bIndex = 0; bIndex < paramRes.length; bIndex++){
                let bOfficeItem = `
                <div class="col-12 col-sm-6 col-md-3 mt-5">
                    <div class="store-item">
                        <p class="store-title"><i class="fa-solid fa-store"></i> Stores - `+ (bIndex + 1) +`</p>
                        <p class="address"><i class="fa-solid fa-location-dot"></i> ` + paramRes[bIndex].address_line + ", " + paramRes[bIndex].city + `</p>
                        <p class="phone"><i class="fa-solid fa-phone-volume"></i> `+ paramRes[bIndex].phone +`</p>
                    </div>
                </div>`

                vOfficeContain.append(bOfficeItem);
            }
            gSrTop.reveal(".store-item", {interval: 200});
        }
        // Hàm xử lí sau khi đăng nhập
        function responseHandler(paramRes){
            "use strict"
            $("#icon-login").addClass("d-none");
            $("#icon-user").removeClass("d-none");
            if(gProductListId != null){
                gNumberProduct.html(gProductListId.length);
            }
            $("#email-header").html(paramRes.email);
            $("#phone-header").html(paramRes.phoneNumber);

            $("#inp-firstname").val(paramRes.firstName);
            $("#inp-lastname").val(paramRes.lastName);
            $("#inp-phone").val(paramRes.phoneNumber);
            $("#inp-email").val(paramRes.email);
            $("#inp-address").val(paramRes.address);
            $("#inp-city").val(paramRes.city);
            $("#inp-country").val(paramRes.country);
            if(paramRes.photo != null){
                $("#user-img").attr("src",gUrlDownloadPhoto + "/" + paramRes.photo);
            }
        }
    
        //Hàm logout
        function redirectToLogin() {
            // Trước khi logout cần xóa token đã lưu trong cookie
            setCookie("token", "", 1);
        }
        //Hàm setCookie
        function setCookie(cname, cvalue) {
            document.cookie = cname + "=" + cvalue + ";" + ";path=/";
        }

    });