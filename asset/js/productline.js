$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    // Khai báo url
    var gUrlProductLine = "http://localhost:8080/shop24h/product-lines";

    var urlInfo = "http://localhost:8080/shop24h/api/noauth/customers";

    var gUrlDownloadPhoto = "http://localhost:8080/shop24h/api/noauth/downloadFile";

    // Lấy token
    const vtoken = getCookie("token");

    // Cấu hình data table
    var vDataTable = $('#product-line-table').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            columns: [
                {data: "id"},
                {data: "productLine"},
                {data: "description"}
            ]
    });
    // Lưu id dòng sản phẩm dc chọn
    var gIdProductLine = ""; 
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading();

    // Tạo sự kiện logout
    $(document).on("click", "#btn-logout", function(){
        redirectToLogin();
    })

    // Tạo sự kiện nhấn dòng trên bảng
    $(document).on("click", "#product-line-table td", function(){
        onBtnRowClick(this);
        $("#myModal").modal("show");
    })
    // Tạo sự kiến nhấn nút thêm product line
    $(document).on("click", "#btn-add", function(){
        onBtnAddClick();
        $("#myModal").modal("show");
    })
    // Tạo sự kiến nhấn nút thêm product line
    $(document).on("click", "#btn-add-product-line", function(){
        onBtnAddProductClick();
    })
    // Tạo sự kiến nhấn nút cập nhật product line
    $(document).on("click", "#btn-update", function(){
        onBtnUpdateProductLineClick();
    })
    // Tạo sự kiến nhấn xác nhận xoá product line
    $(document).on("click", "#btn-delete", function(){
        $("#delete-modal").modal("show");
    })
    // Tạo sự kiến nhấn xác nhận xoá product line
    $(document).on("click", "#btn-confirm-delete", function(){
        onBtnDeleteProductLineClick();
    })
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading(){
        "use strict"
        callApiCheckUser();
        
    }
    // Hàm gọi api lấy toàn bộ dòng sản phẩm
    function callApiGetAllproductLine(){
        $.ajax({
            url: gUrlProductLine,
            type: "GET",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function(responseObject) {
                loadProductToTable(responseObject);
            },
            error: function(er) {
                // errorHandler();
            }
        });
    }
    // Hàm tạo mới dòng sản phẩm
    function callApiCreateProductLine(paramObj){
        "use strict"
        var vUrl = "http://localhost:8080/shop24h/product-lines";
        var vJsonObj = JSON.stringify(paramObj);

        $.ajax({
            url: vUrl,
            type: "POST",
            contentType: "application/json",
            data: vJsonObj,
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function() {
                toastr.success('Create a new product line successully');
                callApiGetAllproductLine();
            },
            error: function() {
                toastr.error('Error! Please try again.');
            }
        });
    }
    // Hàm cập nhật dòng sản phẩm
    function callApiUpdateProductLine(paramObj){
        "use strict"
        var vUrl = "http://localhost:8080/shop24h/product-lines/" + gIdProductLine;
        var vJsonObj = JSON.stringify(paramObj);
        $.ajax({
            url: vUrl,
            type: "PUT",
            contentType: "application/json",
            data: vJsonObj,
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function() {
                toastr.success('Update the product line successully');
                callApiGetAllproductLine();
            },
            error: function() {
                toastr.error('Error! Please try again.');
            }
        });
    }
    // Hàm xoá dòng sản phẩm
    function callApiDeleteProductLine(){
        "use strict"
        var vUrl = "http://localhost:8080/shop24h/product-lines/" + gIdProductLine;
        $.ajax({
            url: vUrl,
            type: "DELETE",
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function() {
                toastr.success('Delete the product line successully');
                callApiGetAllproductLine();
            },
            error: function() {
                toastr.error('Error! Please try again.');
            }
        });
    }
    // Hàm cập nhật dòng
    function onBtnUpdateProductLineClick(){
        "use strict"
        var vProductObj = {
            productLine: "",
            description: "",
        }
        // Thu thập dữ liệu
        readFormData(vProductObj);
        // Xử lí dữ liệu
        var vValid = validateData(vProductObj);
        if(vValid){
            callApiUpdateProductLine(vProductObj);
        }
    }
    // Hàm xoá dòng sản phẩm
    function onBtnDeleteProductLineClick(){
        "use strict"
        $("#modal-delete").modal("hide");
        $("#myModal").modal("hide");
        callApiDeleteProductLine();
    }
    // Hàm kiểm tra đăng nhập
    function callApiCheckUser(){
        $.ajax({
            url: urlInfo,
            type: "GET",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function(responseObject) {
                if(responseObject.roles[0].name == "ROLE_MANAGER"){
                    callApiGetAllproductLine();
                    responseHandler(responseObject);
                }
                else{
                    redirectToLogin();
                }
            },
            error: function() {
                // Khi token hết hạn, AJAX sẽ trả về lỗi khi đó sẽ redirect về trang login để người dùng đăng nhập lại
                redirectToLogin();
            }
        });
    }
    // Hàm xử lí khi nhấn row
    function onBtnRowClick(paramRow){
        "use strict"
        gIdProductLine = "";
        var vDataRow = onRowClick(paramRow);
        if(vDataRow.length > 0){
            loadDataToForm(vDataRow);
        }
    }

    // Hàm xử lí nhấn xoá dòng sản phẩm
    function onIconDeleteRowClick(paramIcon){
        gIdProductLine = "";
        var vDataRow = onRowClick(paramIcon);
        gIdProductLine = vDataRow[0].id;
        $("#modal-delete").modal("show");
    }
    // Hàm xử lí khi nhấn thêm product line
    function onBtnAddClick(){
        "use strict"
        emptyForm();
    }
    // Hàm xử lí khi nhấn thêm dòng sản phẩm
    function onBtnAddProductClick(){
        "use strict"
        var vProductObj = {
            productLine: "",
            description: "",
        }
        // Thu thập dữ liệu
        readFormData(vProductObj);
        // Xử lí dữ liệu
        var vValid = validateData(vProductObj);
        if(vValid){
            callApiCreateProductLine(vProductObj);
        }
    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // Hàm thu thập dữ liệu tạo mới 
    function readFormData(paramObj){
        "use strict"
        paramObj.productLine = $("#inp-product-line").val();
        paramObj.description = $("#inp-desc").val();
    }
    // Hàm xử lí dữ liệu
    function validateData(paramObj){
        "use strict"
        if(paramObj.productLine == ""){
            toastr.error('Please fill out product line');
            $("#inp-product-line").focus();
            return false;
        }
        if(paramObj.description == ""){
            toastr.error('Please fill out description');
            $("#inp-desc").focus();
            return false;
        }
    return true;
    }
    //Hàm logout
    function redirectToLogin() {
        // Trước khi logout cần xóa token đã lưu trong cookie
        setCookie("token", "", 1);
        window.location.href = "./login.html";
    }
    //Hàm setCookie
    function setCookie(cname, cvalue) {
        document.cookie = cname + "=" + cvalue + ";" + ";path=/";
    }
    //Hàm get Cookie đã giới thiệu ở bài trước
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
    // Hàm xử lí sau kiểm tra người dùng thành công
    function responseHandler(paramRes){
        "use strict"
        $("#username").html(paramRes.username);
        $("#user-img").attr("src", gUrlDownloadPhoto +"/"+ paramRes.photo);
    }
    // Hàm ghi dữ liệu vào bảng
    function loadProductToTable(paramRes){
        "use strict"
        vDataTable.clear();
        vDataTable.rows.add(paramRes);
        vDataTable.order( [ 0, 'desc' ] ).draw();
    }

    // Hàm lấy dữ liệu khi chọn row
    function onRowClick(paramRow){
        "use strict"
        var vRow = $(paramRow).closest("tr");
        $("#product-line-table tr").removeClass("active");
        vRow.addClass("active");
        var vDataOnRow = vDataTable.rows(vRow).data();
        return vDataOnRow;
    }
    // Hàm tải dữ liệu lên form
    function loadDataToForm(paramRowData){
        "use strict"
        $("#btn-action").html(`<button id="btn-update" class="btn btn-primary">Update</button>
        <button id="btn-delete" class="btn btn-danger">Delete</button>`);
        gIdProductLine = paramRowData[0].id;
        $("#inp-id").removeClass("d-none");
        $("#label-id").removeClass("d-none");
        $("#inp-id").val(paramRowData[0].id);
        $("#inp-product-line").val(paramRowData[0].productLine);
        $("#inp-desc").val(paramRowData[0].description);
    }
    // Hàm xoá dữ liệu trên form
    function emptyForm(){
        "use strict"
        $("#btn-action").html(`<button id="btn-add-product-line" class="btn btn-info">Add</button>`);
        gIdProductLine = "";

        $("#inp-id").val("");
        $("#inp-id").addClass("d-none");
        $("#label-id").addClass("d-none");
        $("#inp-product-line").val("");
        $("#inp-desc").val("")
    }
})