$(document).ready(function() {
    //----Phần này làm sau khi đã làm trang info.js
    //Kiểm tra token nếu có token tức người dùng đã đăng nhập
    const vtoken = getCookie("token");

    if(vtoken) {
        window.location.href = "./index.html";
    }
    //----Phần này làm sau khi đã làm trang info.js
    $("#icon-close-modal").on("click", function() { 
        $("#modal-noti").addClass("d-none");
    })
    //Sự kiện bấm nút login
    $("#btn-signup").on("click", function() {      

        var vObj = {
            username: "",
            password: "",
            email: "",
            repeatPassword: "",
            isAgree: false,
        }
        vObj.username = $("#inp-username").val().trim();
        vObj.email = $("#inp-email").val().trim();
        vObj.password = $("#inp-password").val().trim();
        vObj.repeatPassword = $("#inp-repeat-password").val().trim();
        vObj.isAgree = $("#inp-check").is(":checked");

        var vValid  = validateData(vObj);
        if(vValid){
            signinForm(vObj);
        }

    });

    function signinForm(paramObj) {
        var vSignupUrl = "http://localhost:8080/shop24h/api/noauth/signup";

        var vJsonObj = JSON.stringify(paramObj);

        $.ajax({
            type: "POST",
            url: vSignupUrl,
            data: vJsonObj, 
            contentType: "application/json",
            cache : false,
            processData: false,
            success: function(res){
                $("#modal-noti").removeClass("d-none");
            },
            error: function(xhr) {
               // Lấy error message
               toastr.error(xhr.responseJSON.message);
            }
        });
    }

    //Hàm get Cookie đã giới thiệu ở bài trước
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
    // Hàm xử lí dữ liệu
    function validateData(vObj){
        "use strict"
        if (vObj.username == ""){
            toastr.error("Please enter username!");
            return false;
        }
        if (vObj.email == ""){
            toastr.error("Please enter email!");
            return false;
        }
        if (vObj.password == ""){
            toastr.error("Please enter password!");
            return false;
        }
        if (vObj.repeatPassword == ""){
            toastr.error("Please enter repeat password!");
            return false;
        }
        if (vObj.password != vObj.repeatPassword){
            toastr.error("Password not matched!");
            return false;
        }
        if (vObj.password.length < 7){
            toastr.error("Pass word too short!");
            return false;
        }
        if (vObj.isAgree == false){
            toastr.error("You need to agree terms and policies!");
            return false;
        }
        return true;
    }
});